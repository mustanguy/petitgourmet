#!/bin/bash
## monitor magento folder and invalidate opcache
/usr/bin/inotifywait -e modify,move \
    -mrq --timefmt %a-%b-%d-%T --format '%w%f %T' \
    --excludei '/(cache|log|session|report|locks|media|skin|tmp)/|\.(xml|html?|css|js|gif|jpe?g|png|ico|te?mp|txt|csv|swp|sql|t?gz|zip|svn?g|git|log|ini|sh|pl)~?' \
    /home/petitgourmetuae/public_html/ | while read line; do
    echo "$line " >> /var/log/zend_opcache_monitor.log
    FILE=$(echo ${line} | cut -d' ' -f1 | sed -e 's/\/\./\//g' | cut -f1-2 -d'.')
    TARGETEXT="(php|phtml)"
    EXTENSION="${FILE##*.}"
  if [[ "$EXTENSION" =~ $TARGETEXT ]];
    then
    su petitgourmetuae -s /bin/bash -c "curl --cookie 'varnish_bypass=1' --silent http://petitgourmetuae.com/AldAXxZxHOhe_opcache_gui.php?page=invalidate&file=${FILE} >/dev/null 2>&1"
  fi
done
