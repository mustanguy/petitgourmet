<?php
namespace Tempocommerce\Customer\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $currentCustomer;

    public function __construct(
        Context $context,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
    )
    {
        $this->currentCustomer = $currentCustomer;
        parent::__construct(
            $context
        );
    }

    public function getCustomer()
    {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    public function getPhoneNumber()
    {
        if($phone = $this->getCustomer()->getCustomAttribute('petit_customer_phone')){
            return $phone->getValue();
        }else{
            return null;
        }
    }
}