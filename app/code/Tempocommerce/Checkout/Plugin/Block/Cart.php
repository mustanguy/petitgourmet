<?php
namespace Tempocommerce\Checkout\Plugin\Block;

class Cart
{
    protected $_helper;

    function __construct(
        \Tempocommerce\Checkout\Helper\Data $helper
    )
    {
        $this->_helper = $helper;
    }

    public function afterGetContinueShoppingUrl(\Magento\Checkout\Block\Cart $subject, $result)
    {
        return $this->_helper->getContinueShoppingUrl();
    }
}