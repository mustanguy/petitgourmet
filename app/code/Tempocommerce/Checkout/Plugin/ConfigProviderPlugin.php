<?php

namespace Tempocommerce\Checkout\Plugin;

class ConfigProviderPlugin extends \Magento\Framework\Model\AbstractModel
{

    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, array $result)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $chiidren_helper = $objectManager->create('\Tempocommerce\Children\Helper\View');
        $items = $result['totalsData']['items'];
        foreach ($items as $itemKey => $item){
            $options = json_decode($item['options']);
            foreach ($options as $optionKey => $option){
                if($option->label == 'child_id'){
                    $options[$optionKey]->label = 'Child Name';
                    $options[$optionKey]->value = $chiidren_helper->getChildName($option->value);
                }
                if($option->label == 'calendar_inputField'){
                    $options[$optionKey]->label = 'Start Date';
                }
            }
            $result['totalsData']['items'][$itemKey]['options'] = json_encode($options);
        }
        return $result;
    }

}