<?php

namespace Tempocommerce\Checkout\Observer\Checkout;

class CartAddProductComplete implements \Magento\Framework\Event\ObserverInterface {

    protected $_responseFactory;
    protected $_url;

    public function __construct(\Magento\Framework\App\ResponseFactory $responseFactory, \Magento\Framework\UrlInterface $url) {
        $this->_responseFactory = $responseFactory;
        $this->_url             = $url;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer
    ) {
        $objectManager   = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        if (!$customerSession->isLoggedIn()) {
            // customer login action
            $customerAuthUrl = $this->_url->getUrl('customer/account/login');
            $this->_responseFactory->create()->setRedirect($customerAuthUrl)->sendResponse();
        }
    }

}
