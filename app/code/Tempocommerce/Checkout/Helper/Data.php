<?php
namespace Tempocommerce\Checkout\Helper;

use Magento\Framework\App\Helper\Context;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    const XML_PATH_CONT_SHOPPING_URL = 'petitsetting/general/continue_shopping';

    protected $_storeManager;

    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * @param $path
     * @param $storeId
     * @return mixed
     */
    public function getConfigValue($path, $storeId)
    {
        return $this->_storeManager->getStore()->getBaseUrl().$this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }


    /**
     * @param $xmlPath
     * @return mixed
     */
    public function getContinueShoppingUrl()
    {
        return $this->getConfigValue(self::XML_PATH_CONT_SHOPPING_URL, 0);
    }
}