<?php
namespace Tempocommerce\Children\Helper;

use Tempocommerce\Children\Api\Data\ChildrenInterface;
use Tempocommerce\Nursery\Api\Data\NurseryInterface;
use Magento\Eav\Model\Config;

class View extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $customerSession;
    protected $_childFactory;
    protected $_nurseryFactory;
    protected $eavConfig;
    protected $objectManager;

    public function __construct(
        \Tempocommerce\Nursery\Model\NurseryFactory $nurseryFactory,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Tempocommerce\Children\Model\ChildrenFactory $childFactory,
        Config $eavConfig
    )
    {
        $this->_nurseryFactory = $nurseryFactory;
        $this->_childFactory = $childFactory;
        $this->customerSession = $customerSession;
        $this->eavConfig = $eavConfig;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($context);
    }

    public function isLoggedIn()
    {
        $context = $this->objectManager->get('Magento\Framework\App\Http\Context');
        $isLoggedIn = $context->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
        return $isLoggedIn;
    }

    public function getLogInUrl()
    {
        return $this->_getUrl('customer/account/login/');
    }

    public function getChildName($id)
    {
        $child  = $this->_childFactory->create()->load($id);
        $fname = $child->getFirstName();
        $lname = $child->getLastName();

        return $fname .' '. $lname;
    }

    public function getNurseryId($id)
    {
        $child  = $this->_childFactory->create()->load($id);
        $nursery = $child->getNursery();

        return $nursery;
    }

    public function getChildNursery($id)
    {
        $nursery = $this->_nurseryFactory->create()->load($id);

        return $nursery->getName();
    }

    public function getChildOptions()
    {
        $options = [];
        $customerSession = $this->objectManager->create('Magento\Customer\Model\Session');
        if($customerSession->isLoggedIn()) {
            $id = $customerSession->getCustomer()->getId();
            $nurseryCollection = $this->_childFactory->create()->getCollection()->addFieldToFilter('parent_id', $id);
            foreach ($nurseryCollection as $item) {
                $options[$item->getChildrenId()] = $item->getFirstName();
            }
        }
        return $options;
    }

    public function getAttributeOptionsAdminValues()
    {
        $values = [];
        $attribute_code = 'no_of_days'; // need to get this from admin config
        $attribute = $this->eavConfig->getAttribute('catalog_product', $attribute_code);
        $attribute->setStoreId(0);
        $options = $attribute->getSource()->getAllOptions();
        foreach ($options as $option) {
            if (!empty($option['value'])) {
                $values ['options'][$option['value']] = $option['label'];
            }
        }
        $values ['attribute_id'] = $attribute->getAttributeId();
        return $values;
    }

    public function orderAdditionalDetails($options){
        $html = '';
        $days = [];
        if ($options && isset($options['info_buyRequest'])) {
            $options = $options['info_buyRequest']['tempocommerce'];
            foreach ($options as $key=>$value) {
                if(!strcmp($key,'child_id')){
                    $html .= '<dt>'.__('Child Name').'</dt>';
                    $html .= '<dd>'.$this->getChildName($value).'<dd>';
                }elseif (!strcmp($key,'calendar_inputField')){
                    $html .= '<dt>'.__('Start Date').'</dt>';
                    $html .= '<dd>'.$value.'</dd>';
                }else{
                    $days[] = $value;
                }
            }
            $html .= '<dt>'.__('Days').'</dt>';
            $html .= '<dd>'.implode(', ',$days).'</dd>';
        }

        return $html;
    }

}