<?php
namespace Tempocommerce\Children\Helper;

use Magento\Framework\App\Helper\Context;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    public function getChildForm($child_id)
    {
        return 'child form';

    }
}