<?php
namespace Tempocommerce\Children\Block\Adminhtml\Children\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('child_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Child Information'));
    }
}