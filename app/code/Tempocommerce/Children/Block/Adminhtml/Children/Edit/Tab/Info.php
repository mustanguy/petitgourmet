<?php
namespace Tempocommerce\Children\Block\Adminhtml\Children\Edit\Tab;

class Info extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    protected $_systemStore;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    )
    {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_child');
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Child Info')]);

        if ($model->getChildrenId()) {
            $fieldset->addField(
                'children_id',
                'hidden',
                [
                    'name' => 'children_id',
                    'title' => 'Children ID'
                ]
            );
        }

        $fieldset->addField(
            'child_id',
            'text',
            [
                'name' => 'child_id',
                'label' => __('Child ID'),
                'title' => __('Child ID'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'parent_id',
            'text',
            [
                'name' => 'parent_id',
                'label' => __('Parent Id'),
                'title' => __('parent_id'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'first_name',
            'text',
            [
                'name' => 'first_name',
                'label' => __('First Name'),
                'title' => __('first_name'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'last_name',
            'text',
            [
                'name' => 'last_name',
                'label' => __('Last Name'),
                'title' => __('last_name'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'birth_date',
            'text',
            [
                'name' => 'birth_date',
                'label' => __('Birth Date'),
                'title' => __('birth_date'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'allergies',
            'text',
            [
                'name' => 'allergies',
                'label' => __('Allergies'),
                'title' => __('allergies'),
                'required' => true
            ]
        );


        if ($model) {
            $form->setValues($model->getData());
        }
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * @return \Magento\Framework\Phrase|string
     */
    public function getTabLabel()
    {
        return __('Child Info');
    }

    /**
     * @return \Magento\Framework\Phrase|string
     */
    public function getTabTitle()
    {
        return __('Child Info');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}