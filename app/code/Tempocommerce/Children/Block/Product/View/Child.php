<?php

namespace Tempocommerce\Children\Block\Product\View;

use Tempocommerce\Children\Model;

class Child extends \Magento\Framework\View\Element\Template
{
    protected $_nurseryFactory;
    protected $request;
    protected $_child;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Tempocommerce\Nursery\Model\NurseryFactory $nurseryFactory,
        \Magento\Backend\Block\Template\Context $context,
        Model\ChildrenRepository $child,
        array $data = []
    )
    {
        $this->_nurseryFactory = $nurseryFactory;
        $this->_child = $child;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve form action
     *
     * @return string
     */
    public function getAjaxUrl()
    {
        return $this->getUrl("children/index/save");
    }

    public function getProfileForm()
    {
        return $this->getUrl("children/index/form");
    }

    public function getNurseryOptions()
    {
        $nurseryCollection = $this->_nurseryFactory->create()->getCollection();
        $options = [];
        foreach ($nurseryCollection as $item) {
            $options[$item->getNurseryId()] = $item->getName();
        }
        return $options;
    }

    public function getContactUrl()
    {
        return $this->getUrl("contact");
    }

    public function getChildValues()
    {
        $data = null;
        $params =  $this->getRequest()->getParams();
        if(!empty($params)) {
            $id = $this->getRequest()->getParams()['id'];
            if ($id) {
                $data = $this->_child->getById($id)->getData();
            }
        }
        return $data;
    }

    public function getBackUrl(){
        return $this->getUrl("children/profile");
    }
}