<?php
namespace Tempocommerce\Children\Block\Product\View;

use Magento\Framework\Json\Encoder;

class Form extends \Magento\Framework\View\Element\Template{

    /**
     * @var array
     */
    protected $layoutProcessors;

    /**
     * @var Encoder
     */
    protected $jsonEncoder;

    /**
     * @var CompositeConfigProvider
     */
    protected $configProvider;

    /**
     * Form constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param Encoder $jsonEncoder
     * @param array $layoutProcessors
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Encoder $jsonEncoder,
        array $layoutProcessors = [],
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->layoutProcessors = $layoutProcessors;
        $this->jsonEncoder = $jsonEncoder;

        if (isset($data['jsLayout'])) {
            $this->jsLayout = $data['jsLayout'];
            unset($data['jsLayout']);
        }
    }

    /**
     * @return string
     */
    public function getJsLayout()
    {
        foreach ($this->layoutProcessors as $processor) {
            $this->jsLayout = $processor->process($this->jsLayout);
        }
        return parent::getJsLayout();
    }
}