<?php

namespace Tempocommerce\Children\Block\Layout;

class DataProcessor extends \Magento\Framework\View\Element\Template implements LayoutProcessorInterface
{

    protected $form;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Tempocommerce\Children\Model\Form $form
    )
    {
        $this->form = $form;

        parent::__construct($context);
    }

    public function process($jsLayout)
    {
        if (isset($jsLayout['components']['productRequest'])) {
            if (!isset($jsLayout['components']['productRequest']['config'])) {
                $jsLayout['components']['productRequest']['config'] = [];
            }
            $jsLayout['components']['productRequest']['config'] = $this->additionalData($jsLayout['components']['productRequest']['config']);
        }
        return $jsLayout;
    }

    /**
     * @param $obj
     */
    public function additionalData($obj)
    {
        $key = 'settings';
        //$obj[$key]['popup_description'] = $this->config->getPopupDescription();
        $obj[$key]['ajaxUrl'] = $this->getUrl('children/index/index');
        $obj[$key]['ajaxPostUrl'] = $this->getUrl('children/index/indexPost');
        $product = $this->form->getProduct();
        $obj[$key]['product_name'] = $product ? $product->getName() : '';
        $obj[$key]['product_id'] = $product ? $product->getId() : '';
        return $obj;
    }
}