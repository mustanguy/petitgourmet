<?php
namespace Tempocommerce\Children\Block\Layout;

interface LayoutProcessorInterface
{
    public function process($jsLayout);
}