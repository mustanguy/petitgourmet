define(
    [
        'jquery',
        'ko',
        'uiComponent',
        'colorbox'
    ],
    function ($, ko, Component, colorbox) {
        "use strict";

        var customerDetails = ko.observable();

        return Component.extend({
            defaults: {
                settings: {
                    ajaxUrl: '#',
                    method: 'POST',
                    popupContainer: '#add-child-popup-container',
                    msgContainer: '.response-messages',
                    requestForm: '#popup-add-child-form',
                    requestLink: '#add-child-link',
                    close : '#add-child-popup-colse'
                },
                //colorbox: {
                //    status: '1' //1 is open 0 is closed
                //}
            },

            customerDetails: customerDetails,

            initialize: function () {
                var _self = this;
                this._super();
                this.bindEvents();
            },

            bindEvents: function () {
                var _self = this;
                $(document).on('submit', this.settings.requestForm, function (event) {
                    event.preventDefault();
                    if (_self.validateForm(event.target)) {
                        if (_self.isAlreadyAddedProduct()) {
                            $(_self.settings.popupContainer).find(_self.settings.msgContainer).text('Already Requested');
                        } else {
                            _self.submitFormAjax(event.target);
                        }
                    }
                });

                $(this.settings.requestLink).bind('click', function (event) {
                    event.preventDefault();
                    _self.getCustomerDetails();
                });

                $(document).on('click', this.settings.close, function (event) {
                    _self.closePopup(_self.settings.popupContainer);
                });
            },

            submitFormAjax: function (formElement) {
                var form = $(formElement);
                var _self = this;
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function(response) {
                        if (response) {
                            if (response.status == 'success') {
                                $(_self.settings.popupContainer).find(_self.settings.msgContainer).html(response.data);
                                _self.closePopup(_self.settings.popupContainer);
                                _self.showPopup($('<div class="tempPopup">' + response.data + "</div>"));
                                _self.setAlreadyAddedProduct();
                            } else {
                                $(_self.settings.popupContainer).find(_self.settings.msgContainer).html(response.data);
                            }
                        }
                    }
                });
            },

            getCustomerDetails: function () {
                var _self = this;
                $.ajax({
                    url: _self.settings.ajaxUrl,
                    type: _self.settings.method,
                    dataType: 'json',
                    complete: function (data) {
                        $('body').trigger('miniex_ajax_complete');
                        try {
                            var responseData = JSON.parse(data.responseText);
                            var settings = $.extend(responseData.data, _self.settings);
                            customerDetails(settings);

                            setTimeout(function(){
                                _self.showPopup(_self.settings.popupContainer);
                            }, 500);
                        } catch (e){
                            console.log('error');
                        }
                    }
                });
                return customerDetails;
            },

            showPopup: function (element) {
                $('body').addClass('popup-scroll-stop');
                $(element).dialog({
                    close: function( event, ui ) {
                        $('body').removeClass('popup-scroll-stop');
                    }
                });
                jQuery('.ui-dialog').css('z-index',999);
                //var colorboxSettings = {
                //    html: html,
                //    onOpen: $.proxy(_self.colorboxOpenCallback, _self),
                //    onClosed: $.proxy(_self.colorboxClosedCallback, _self)
                //};
                //if (typeof settings != 'undefined') {
                //    colorboxSettings = $.extend(colorboxSettings, settings);
                //}
                //$.colorbox(colorboxSettings);
            },

            closePopup: function (element) {
                $('body').removeClass('popup-scroll-stop');
                $(element).dialog('close');
            },

            validateForm: function (form) {
                return $(form).validation() && $(form).validation('isValid');
            },

            //colorboxOpenCallback: function (e){
            //    var _self = this;
            //    _self.colorbox.status = 1;
            //},
            //
            //colorboxClosedCallback: function (e) {
            //    var _self = this;
            //    _self.colorbox.status = 0;
            //},

            isAlreadyAddedProduct: function () {
                //$.cookie("test", 1, { expires : 10 });
                var requestedProducts = $.cookie('product_request');
                if (typeof requestedProducts !== 'undefined') {
                    if (requestedProducts) {
                        var productIds = requestedProducts.split(',');
                        if ($.inArray($('#pr_product_id').val(), productIds) !== -1) {
                            return true;
                        }
                    }
                }
                return false;
            },

            setAlreadyAddedProduct: function () {
                var requestedProducts = $.cookie('product_request');
                if (typeof requestedProducts !== 'undefined') {
                    requestedProducts = requestedProducts + ',' + $('#pr_product_id').val();
                }
                $.cookie("product_request", requestedProducts, { expires : 1 });
            }
        });
    }
);