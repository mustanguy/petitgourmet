<?php

namespace Tempocommerce\Children\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $tableName = $setup->getTable('tempocommerce_children');

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            // Changes from version 1.0.0 listed here.
            //creation of additional children Module arguments
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName, 'parent_id',
                    ['type' => Table::TYPE_INTEGER, 'nullable' => false, 'default' => 0,
                        'comment' => 'Parent Id']
                );

                $connection->addColumn(
                    $tableName, 'first_name',
                    ['type' => Table::TYPE_TEXT, 'nullable' => false, 'default' => '',
                        'comment' => 'First Name']
                );

                $connection->addColumn(
                    $tableName, 'last_name',
                    ['type' => Table::TYPE_TEXT, 'nullable' => false, 'default' => '',
                        'comment' => 'Last Name']
                );

                $connection->addColumn(
                    $tableName, 'birth_date',
                    ['type' => Table::TYPE_TIMESTAMP, 'nullable' => false, 'default' => '',
                        'comment' => 'Birth Date']
                );

                $connection->addColumn(
                    $tableName, 'allergies',
                    ['type' => Table::TYPE_TEXT, 'nullable' => TRUE, 'default' => '',
                        'comment' => 'Updated At']
                );

                $connection->addColumn(
                    $tableName, 'created_at',
                    ['type' => Table::TYPE_TIMESTAMP, 'nullable' => TRUE, 'default' => '',
                        'comment' => 'Created At']
                );

                $connection->addColumn(
                    $tableName, 'updated_at',
                    ['type' => Table::TYPE_TIMESTAMP, 'nullable' => false, 'default' => '',
                        'comment' => 'Updated At']
                );
            }
        }
        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName, 'delivery_days',
                    ['type' => Table::TYPE_TEXT, 'nullable' => true, 'default' => '',
                        'comment' => 'Delivery Days']
                );
                $connection->addColumn(
                    $tableName, 'nursery',
                    ['type' => Table::TYPE_TEXT, 'nullable' => true, 'default' => '',
                        'comment' => 'Nursery']
                );

                $connection->addColumn(
                    $tableName, 'class_num',
                    ['type' => Table::TYPE_TEXT, 'nullable' => true, 'default' => '',
                        'comment' => 'Class Num']
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                if ($connection->tableColumnExists($tableName, 'child_id')) {
                    $connection->dropColumn($tableName, 'child_id');
                }
                if ($connection->tableColumnExists($tableName, 'birth_date')) {
                    $connection->dropColumn($tableName, 'birth_date');
                }
                if ($connection->tableColumnExists($tableName, 'created_at')) {
                    $connection->dropColumn($tableName, 'created_at');
                }
                if ($connection->tableColumnExists($tableName, 'updated_at')) {
                    $connection->dropColumn($tableName, 'updated_at');
                }
                $connection->addColumn(
                    $tableName, 'birth_date',
                    ['type' => Table::TYPE_DATE, 'nullable' => true, 'default' => null,
                        'comment' => 'Birth Day']
                );
                $connection->addColumn(
                    $tableName, 'start_date',
                    ['type' => Table::TYPE_DATE, 'nullable' => true, 'default' => null,
                        'comment' => 'Start Date']
                );
                $connection->addColumn(
                    $tableName, 'updated_at',
                    ['type' => Table::TYPE_TIMESTAMP, 'nullable' => false, 'default' => '',
                        'comment' => 'Updated At']
                );
                $connection->addColumn(
                    $tableName, 'created_at',
                    ['type' => Table::TYPE_TIMESTAMP, 'nullable' => false, 'default' => '',
                        'comment' => 'Created At']
                );
            }
        }
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                if ($connection->tableColumnExists($tableName, 'start_date')) {
                    $connection->dropColumn($tableName, 'start_date');
                }
                if ($connection->tableColumnExists($tableName, 'delivery_days')) {
                    $connection->dropColumn($tableName, 'delivery_days');
                }
            }
        }
        $setup->endSetup();

    }

}
