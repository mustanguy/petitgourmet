<?php


namespace Tempocommerce\Children\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ChildrenRepositoryInterface
{


    /**
     * Save Children
     * @param \Tempocommerce\Children\Api\Data\ChildrenInterface $children
     * @return \Tempocommerce\Children\Api\Data\ChildrenInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function save(
        \Tempocommerce\Children\Api\Data\ChildrenInterface $children
    );

    /**
     * Retrieve Children
     * @param string $childrenId
     * @return \Tempocommerce\Children\Api\Data\ChildrenInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function getById($childrenId);

    /**
     * Retrieve Children matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Tempocommerce\Children\Api\Data\ChildrenSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Children
     * @param \Tempocommerce\Children\Api\Data\ChildrenInterface $children
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function delete(
        \Tempocommerce\Children\Api\Data\ChildrenInterface $children
    );

    /**
     * Delete Children by ID
     * @param string $childrenId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function deleteById($childrenId);
}
