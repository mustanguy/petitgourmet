<?php


namespace Tempocommerce\Children\Api\Data;

interface ChildrenSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get Children list.
     * @return \Tempocommerce\Children\Api\Data\ChildrenInterface[]
     */
    
    public function getItems();

    /**
     * Set child_id list.
     * @param \Tempocommerce\Children\Api\Data\ChildrenInterface[] $items
     * @return $this
     */
    
    public function setItems(array $items);
}
