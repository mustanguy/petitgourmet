<?php


namespace Tempocommerce\Children\Api\Data;

interface ChildrenInterface
{

    const CHILDREN_ID = 'children_id';
    const CHILD_ID = 'child_id';


    /**
     * Get children_id
     * @return string|null
     */
    
    public function getChildrenId();

    /**
     * Set children_id
     * @param string $children_id
     * @return Tempocommerce\Children\Api\Data\ChildrenInterface
     */
    
    public function setChildrenId($childrenId);

    /**
     * Get child_id
     * @return string|null
     */
    
    public function getChildId();

    /**
     * Set child_id
     * @param string $child_id
     * @return Tempocommerce\Children\Api\Data\ChildrenInterface
     */
    
    public function setChildId($child_id);
}
