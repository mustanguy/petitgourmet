<?php


namespace Tempocommerce\Children\Model\ResourceModel;

class Children extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('tempocommerce_children', 'children_id');
    }
}
