<?php


namespace Tempocommerce\Children\Model\ResourceModel\Children;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Tempocommerce\Children\Model\Children',
            'Tempocommerce\Children\Model\ResourceModel\Children'
        );
    }
}
