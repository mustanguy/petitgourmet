<?php


namespace Tempocommerce\Children\Model;

use Tempocommerce\Children\Api\Data\ChildrenInterface;

class Children extends \Magento\Framework\Model\AbstractModel implements ChildrenInterface
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tempocommerce\Children\Model\ResourceModel\Children');
    }

    /**
     * Get children_id
     * @return string
     */
    public function getChildrenId()
    {
        return $this->getData(self::CHILDREN_ID);
    }

    /**
     * Set children_id
     * @param string $childrenId
     * @return Tempocommerce\Children\Api\Data\ChildrenInterface
     */
    public function setChildrenId($childrenId)
    {
        return $this->setData(self::CHILDREN_ID, $childrenId);
    }

    /**
     * Get child_id
     * @return string
     */
    public function getChildId()
    {
        return $this->getData(self::CHILD_ID);
    }

    /**
     * Set child_id
     * @param string $child_id
     * @return Tempocommerce\Children\Api\Data\ChildrenInterface
     */
    public function setChildId($child_id)
    {
        return $this->setData(self::CHILD_ID, $child_id);
    }
}
