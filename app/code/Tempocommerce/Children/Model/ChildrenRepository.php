<?php


namespace Tempocommerce\Children\Model;

use Tempocommerce\Children\Api\ChildrenRepositoryInterface;
use Tempocommerce\Children\Api\Data\ChildrenSearchResultsInterfaceFactory;
use Tempocommerce\Children\Api\Data\ChildrenInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Tempocommerce\Children\Model\ResourceModel\Children as ResourceChildren;
use Tempocommerce\Children\Model\ResourceModel\Children\CollectionFactory as ChildrenCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class ChildrenRepository implements ChildrenRepositoryInterface
{

    protected $resource;

    protected $ChildrenFactory;

    protected $ChildrenCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataChildrenFactory;

    private $storeManager;


    /**
     * @param ResourceChildren $resource
     * @param ChildrenFactory $childrenFactory
     * @param ChildrenInterfaceFactory $dataChildrenFactory
     * @param ChildrenCollectionFactory $childrenCollectionFactory
     * @param ChildrenSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceChildren $resource,
        ChildrenFactory $childrenFactory,
        ChildrenInterfaceFactory $dataChildrenFactory,
        ChildrenCollectionFactory $childrenCollectionFactory,
        ChildrenSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->childrenFactory = $childrenFactory;
        $this->childrenCollectionFactory = $childrenCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataChildrenFactory = $dataChildrenFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Tempocommerce\Children\Api\Data\ChildrenInterface $children
    ) {
        /* if (empty($children->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $children->setStoreId($storeId);
        } */
        try {
            $this->resource->save($children);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the children: %1',
                $exception->getMessage()
            ));
        }
        return $children;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($childrenId)
    {
        $children = $this->childrenFactory->create();
        $children->load($childrenId);
        if (!$children->getId()) {
            throw new NoSuchEntityException(__('Children with id "%1" does not exist.', $childrenId));
        }
        return $children;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $collection = $this->childrenCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $items = [];
        
        foreach ($collection as $childrenModel) {
            $childrenData = $this->dataChildrenFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $childrenData,
                $childrenModel->getData(),
                'Tempocommerce\Children\Api\Data\ChildrenInterface'
            );
            $items[] = $this->dataObjectProcessor->buildOutputDataArray(
                $childrenData,
                'Tempocommerce\Children\Api\Data\ChildrenInterface'
            );
        }
        $searchResults->setItems($items);
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Tempocommerce\Children\Api\Data\ChildrenInterface $children
    ) {
        try {
            $this->resource->delete($children);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Children: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($childrenId)
    {
        return $this->delete($this->getById($childrenId));
    }
}
