<?php


namespace Tempocommerce\Children\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Tempocommerce\Children\Helper\Data;

class Form extends Action
{


    protected $request;
    protected $_helper;
    protected $_session;

    public function __construct(
        Context $context,
        Data $helper,
        \Magento\Customer\Model\Session $session
    )
    {
        $this->_session = $session;
        $this->_helper = $helper;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_url->getUrl('customer/account/login'));
        if ($this->_session->authenticate()) {
            $child_id = $this->getRequest()->getPost('child_id');
            if (!is_null($child_id)) {
                try {
                    $html = $this->_helper->getChildForm($child_id);
                    $resultJson->setData($html);
                }catch (\Exception $e){
                    $resultJson->setData('There are some issues with this order, please contact the admin.');
                }
            }else{
                return $resultRedirect;
            }
        }else {
            /* $response['errors'] = true;
             $response['message'] = 'Error Here';
             $response['redirectUrl'] = $this->_url->getUrl('customer/account/login');
             $resultJson->setData($response);*/
            return $resultRedirect;
        }

        return $resultJson;
    }
}
