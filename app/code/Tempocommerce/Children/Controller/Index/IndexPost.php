<?php
namespace Tempocommerce\Children\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\Result\JsonFactory;


class IndexPost extends Action
{

    protected $_resultPageFactory;


    protected $children;


    protected $childrenRepository;


    protected $productRepository;


    protected $email;


    protected $config;


    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Tempocommerce\Children\Model\Children $children,
        \Tempocommerce\Children\Model\ChildrenRepository $childrenRepository,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->children = $children;
        $this->childrenRepository = $childrenRepository;

        parent::__construct($context);
    }


    public function execute()
    {
        $output = new \stdClass();

        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        if ($this->getRequest()->isPost()) {
            if ($data = $this->getRequest()->getParam('data')) {
                if ($data && !empty($data['product_id'])) {
                    try {
                        $product = $this->productRepository->getById($data['product_id']);
                        if ($product && $product->getId()) {
                            $item = $this->children->create();
                            $data['product_sku'] = $product->getSku();
                            $item->setData($data);
                            $saved = $this->childrenRepository->save($item);

                            if ($saved->getId()) {
                               // $this->email->sendEmailRequestReceived($item, $product);

                                $output->status = 'success';
                                $output->data = 'Success msg';
                                //$output->data = $this->config->getPopupSuccessMessage();
                            }
                        } else {
                            // no product found
                        }
                    } catch (\Exception $e) {
                        $output->status = 'error';
                        $output->data = 'Error Occured';
                    }
                } else {
                    //no product
                    $output->status = 'error';
                    $output->data = 'No Product Found';
                }

            }
        }

        return $result->setData($output);
    }

    /**
     * @param $data
     */
    public function Validate($data)
    {

    }
}