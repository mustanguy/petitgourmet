<?php
namespace Tempocommerce\Children\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{

    protected $_resultPageFactory;


    protected $formModel;


    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Tempocommerce\Children\Model\Form $formModel
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->formModel = $formModel;
        parent::__construct($context);
    }


    public function execute()
    {
        if ($this->getRequest()->isPost()) {
            $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);

            $o = new \stdClass();
            $o->status = 'success';
            $o->data = $this->getDataFromCustomer();

            return $result->setData($o);
        } else {
            $this->_forward('*/*/*');
        }
    }


    public function getDataFromCustomer()
    {
        $data = [];
        $customer = $this->formModel->getCustomer();
        if ($customer) {
            $data['requested_by'] = $customer->getName();
            $data['email'] = $customer->getEmail();
            $data['telephone'] = $customer->getDefaultBillingAddress() ? $customer->getDefaultBillingAddress()->getTelephone() : '';
        } else {
            $data['requested_by'] = '';
            $data['email'] = '';
            $data['telephone'] = '';
        }
        return $data;
    }
}
