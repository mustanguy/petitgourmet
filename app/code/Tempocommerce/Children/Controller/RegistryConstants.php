<?php

namespace Tempocommerce\Children\Controller;

/**
 * Declarations of core registry keys used by the Children module
 *
 */
class RegistryConstants
{
    /**
     * Registry key where current child ID is stored
     */
    const CURRENT_CHILD_ID = 'current_child_id';
}
