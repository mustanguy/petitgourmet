<?php
namespace Tempocommerce\Children\Controller\Adminhtml\Children;


class Save extends Index
{

    public function execute()
    {

        $childData = $this->getRequest()->getPostValue();

        if ($childData) {
            $childrenId = $this->getRequest()->getParam('children_id');
            $childModel = $this->_objectManager->create('Tempocommerce\Children\Model\Children');

            if ($childrenId) {
                $childModel->load($childrenId);
                if ($childrenId != $childModel->getChildrenId()) {
                    throw new \Magento\Framework\Exception\LocalizedException(__('The wrong item is specified.'));
                }
            }

            if (!$this->validate($childData)) {
                $this->_redirect('*/*/edit', ['id' => $childModel->getChildrenId(), '_current' => true]);
                return;
            }

            try {
                $childModel->setData($childData)->save();
                $this->messageManager->addSuccessMessage(__('Child has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['id' => $childModel->getId(), '_current' => true]);
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (\Magento\Framework\Exception\LocalizedException  $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the child.'));
            }

            $this->_getSession()->setFormData($childData);
            $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('children_id')]);
            return;
        }
        $this->_redirect('*/*/');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Tempocommerce_Children::Children_savee');
    }

    /**
     * @param $data
     * @return mixed
     */
    public function validate($data)
    {
        $inputFilter = new \Zend_Filter_Input(
            [],
            [],
            $data
        );
        return $inputFilter->getUnescaped();
    }

}