<?php
namespace Tempocommerce\Children\Controller\Adminhtml\Children;

use Tempocommerce\Children\Controller\RegistryConstants;
use Tempocommerce\Children\Api\ChildrenRepositoryInterface;

class Index extends \Magento\Backend\App\Action
{
    protected $_coreRegistry = null;
    protected $_childRepository;
    protected $_viewHelper;
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry,
        //ChildrenRepositoryInterface $childRepository,
        \Tempocommerce\Children\Helper\View $viewHelper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        //$this->_childRepository = $childRepository;
        $this->_viewHelper = $viewHelper;
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->prepend(__("Children"));
            return $resultPage;
    }

    /**
     * Prepare child default title
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return void
     */
    protected function prepareDefaultCustomerTitle(\Magento\Backend\Model\View\Result\Page $resultPage)
    {
        $resultPage->getConfig()->getTitle()->prepend(__('Child'));
    }

    protected function initCurrentChild()
    {
        $childId = (int)$this->getRequest()->getParam('id');

        if ($childId) {
            $this->_coreRegistry->register(RegistryConstants::CURRENT_CHILD_ID, $childId);
        }

        return $childId;
    }
}
