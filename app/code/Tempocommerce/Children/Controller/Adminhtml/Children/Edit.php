<?php
namespace Tempocommerce\Children\Controller\Adminhtml\Children;

class Edit extends Index
{

    public function execute()
    {
        $childId = $this->initCurrentChild();

        $childData = [];
        $child = null;

        $isExistingChild = (bool)$childId;
        $childData['children_id'] = $childId;
        $this->_getSession()->setChildData($childData);

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Tempocommerce_Children::Children_update');
        $this->prepareDefaultCustomerTitle($resultPage);
        $resultPage->setActiveMenu('Tempocommerce_Children::Children_update');
        if ($isExistingChild) {
            $model = $this->_objectManager->create('Tempocommerce\Children\Model\Children');
            $this->_coreRegistry->register('current_child', $model->load($childId));
        } else {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('tempocommerce_children/children/index');
        }
        return $resultPage;
    }
}