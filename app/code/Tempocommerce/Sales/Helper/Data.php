<?php
namespace Tempocommerce\Sales\Helper;

use Magento\Framework\App\Helper\Context;
use DateTime;
use DateInterval;
use DatePeriod;

class Data extends \Magento\Framework\App\Helper\AbstractHelper{

    protected $_childFactory;
    protected $_nurseryFactory;
    protected $eavConfig;

    public function __construct(
        \Tempocommerce\Children\Model\ChildrenFactory $childFactory,
        \Tempocommerce\Nursery\Model\NurseryFactory $nurseryFactory,
        \Magento\Eav\Model\Config $eavConfig,
        Context $context
    )
    {
        $this->_nurseryFactory = $nurseryFactory;
        $this->_childFactory = $childFactory;
        $this->eavConfig = $eavConfig;
        parent::__construct($context);
    }


    public function orderAdditionalDetails($options){
        $html = '';
        $days = [];
        if ($options && isset($options['info_buyRequest'])) {
            $options = $options['info_buyRequest']['tempocommerce'];
            foreach ($options as $key=>$value) {
                if(!strcmp($key,'child_id')){
                    $html .= '<dt style="padding-left: 20px;">'.__('Child Name').'</dt>';
                    $html .= '<dd style="padding-left: 30px;">'.$this->getChildName($value).'<dd>';
                    $html .= '<dt style="padding-left: 20px;">'.__('Child Nursery').'</dt>';
                    $html .= '<dd style="padding-left: 30px;">'.$this->getChildNursery($this->getNurseryId($value)).'<dd>';
                }elseif (!strcmp($key,'calendar_inputField')){
                    $html .= '<dt style="padding-left: 20px;">'.__('Start Date').'</dt>';
                    $html .= '<dd style="padding-left: 30px;">'.$value.'</dd>';
                }else{
                    $days[] = $value;
                }
            }
            $html .= '<dt style="padding-left: 20px;">'.__('Days').'</dt>';
            $html .= '<dd style="padding-left: 30px;">'.implode(', ',$days).'</dd>';
        }

        return $html;
    }

    public function orderAdditionalDetailsArray($options){
        $data = [
            'end_date' => '',
            'child_name' => '',
            'start_date' => ''
        ];
        if ($options && isset($options['info_buyRequest'])) {
            $data['end_date'] = $this->getEndDate($options['info_buyRequest']);
            $options = $options['info_buyRequest']['tempocommerce'];
            foreach ($options as $key=>$value) {
                if(!strcmp($key,'child_id')){
                    $data['child_name'] = $this->getChildName($value);
                }elseif (!strcmp($key,'calendar_inputField')){
                    $data['start_date'] = $value;
                }else{
                    $days[] = $value;
                }
            }
        }
        return $data;
    }

    public function getEndDate($info_buyRequest)
    {
        $numberOfWeeks = null;
        $subscriptionAttr = $this->eavConfig->getAttribute('catalog_product', 'subscription');
        $subscriptionOps = $subscriptionAttr->setStoreId(0)->getSource()->getAllOptions();
        $subAttId = $subscriptionAttr->getAttributeId();
        $atts = $info_buyRequest['super_attribute'];
        $subOpId = $atts[$subAttId];
        foreach ($subscriptionOps as $subscriptionOp) {
            if ($subscriptionOp['value'] === $subOpId) {
                $numberOfWeeks = (int)$subscriptionOp['label'];
            }
        }

        $startDate = $info_buyRequest['tempocommerce']['calendar_inputField'];
        switch ($numberOfWeeks) {
            case 1:
                $daysMargin = 'P7D';
                break;
            case 2:
                $daysMargin = 'P14D';
                break;
            case 3:
                $daysMargin = 'P21D';
                break;
            case 4:
                $daysMargin = 'P28D';
                break;
            case 5:
                $daysMargin = 'P35D';
                break;
            case 6:
                $daysMargin = 'P42D';
                break;
            case 7:
                $daysMargin = 'P49D';
                break;
            case 8:
                $daysMargin = 'P56D';
                break;
            case 9:
                $daysMargin = 'P63D';
                break;
            case 10:
                $daysMargin = 'P70D';
                break;
            case 11:
                $daysMargin = 'P77D';
                break;
            case 12:
                $daysMargin = 'P84D';
                break;
            case 13:
                $daysMargin = 'P91D';
                break;
            case 14:
                $daysMargin = 'P98D';
                break;
            case 15:
                $daysMargin = 'P105D';
                break;
            case 16:
                $daysMargin = 'P112D';
                break;
            default:
                $daysMargin = 'P0D';
        }
        $startDateObj = new DateTime($startDate);
        $endDateObjIni = new DateTime($startDate);
        $endDateObj = $endDateObjIni->add(new DateInterval($daysMargin))->modify('-1 day');
        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($startDateObj, $interval, $endDateObj);
        $options = $info_buyRequest['tempocommerce'];
        $days = $this->getDays($options);

        $deliveryDates = [];
        foreach ($daterange as $date) {
            $day = strtolower(date('D', strtotime($date->format('Ymd'))));
            if (in_array($day, $days)) {
                $deliveryDates[] = $date->format('D, d M Y');
            }

        }

        return end($deliveryDates);
    }

    public function getDays($options)
    {
        $days = [];
        $days['sun'] = isset($options['sun']) ? $options['sun'] : null;
        $days['mon'] = isset($options['mon']) ? $options['mon'] : null;
        $days['tue'] = isset($options['tue']) ? $options['tue'] : null;
//        $days['wed'] = isset($options['wed']) ? $options['wed'] : null;
//        $days['thu'] = isset($options['thu']) ? $options['thu'] : null;
        return array_filter($days);
    }

    public function getChildName($id)
    {
        $child  = $this->_childFactory->create()->load($id);
        $fname = $child->getFirstName();
        $lname = $child->getLastName();

        return $fname .' '. $lname;
    }

    public function getNurseryId($id)
    {
        $child  = $this->_childFactory->create()->load($id);
        $nursery = $child->getNursery();

        return $nursery;
    }

    public function getChildNursery($id)
    {
        $nursery = $this->_nurseryFactory->create()->load($id);

        return $nursery->getName();
    }
}