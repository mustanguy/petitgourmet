<?php
namespace Tempocommerce\Menuplanner\Helper;

use DateTime;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_orderCollectionFactory;
    protected $orders;
    protected $customerSession;
    protected $_productCollectionFactory;
    protected $eavConfig;
    protected $menuplanner;
    protected $children;
    protected $checkout_helper;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Tempocommerce\Menuplanner\Model\ResourceModel\Menuplanner\CollectionFactory $menuplanner,
        \Tempocommerce\Children\Helper\View $children,
        \Tempocommerce\Checkout\Helper\Data $checkout_helper
    )
    {
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->customerSession = $customerSession;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->eavConfig = $eavConfig;
        $this->menuplanner = $menuplanner;
        $this->children = $children;
        $this->checkout_helper = $checkout_helper;
        parent::__construct($context);
    }


    public function getMenuPlaner($child_id)
    {
        $readyToPlanHtml = '';
        $prosessingHtml = '';
        $exDate = ['fri', 'sat', 'sun'];
        $today = date('Y-m-d');
        $today = strtolower(date("D", strtotime($today)));
	
        if (in_array($today, $exDate)) {
            $thisMove = '6 days';
            $nextMove = '6 days';
        } elseif($today == "mon" || $today == "tue") {
	    $thisMove = '-3 days';
            $nextMove = '-3 days';
	} else {
            $thisMove = '1 days';
            $nextMove = '1 days';
        }

        $thisWeekStart = date('Ymd', strtotime('monday this week'));
        $thisWeekEnd = date('Ymd', strtotime('sunday this week'));
        $thisWeekStart = new DateTime($thisWeekStart);
        $thisWeekEnd = new DateTime($thisWeekEnd);
        $thisWeekStartDate = $thisWeekStart->modify($thisMove);
        $thisWeekEndDate = $thisWeekEnd->modify($thisMove);

        $nextWeekStart = date('Ymd', strtotime('monday next week'));
        $nextWeekEnd = date('Ymd', strtotime('sunday next week'));
        $nextWeekStart = new DateTime($nextWeekStart);
        $nextWeekEnd = new DateTime($nextWeekEnd);
        $nextWeekStartDate = $nextWeekStart->modify($nextMove);
        $nextWeekEndDate = $nextWeekEnd->modify($nextMove);

        $readyToPlanItems = $this->getReadyToPlanItems($child_id, $nextWeekStartDate, $nextWeekEndDate);
        $prosessingItems = $this->getProsessingItems($child_id, $thisWeekStartDate, $thisWeekEndDate);

        if (!empty($readyToPlanItems)) {
            $readyToPlanHtml .= $this->getReadyToPlanHtml($readyToPlanItems);
        }

        if (!empty($prosessingItems)) {
            $prosessingHtml .= $this->getProsessingHtml($prosessingItems);
        }

        if(empty($readyToPlanItems) AND empty($prosessingItems)){
            $prosessingHtml .= '<p>There is no order for '.$this->children->getChildName($child_id).'. 
            If you would like to order please click on the link <a href='.$this->checkout_helper->getContinueShoppingUrl().'>Order</a></p>';
        }

        return $readyToPlanHtml . $prosessingHtml;

    }

    public function getReadyToPlanHtml($readyToPlanItems)
    {
        $readyToPlanHtml = '';
        $url = $this->getPostUrl();
        $readyToPlanHtml .= '<p>Plan the menu for next week</p>';
        $readyToPlanHtml .= "<form id='menu-planer' action='$url' method='post' id='menu-planer'>";
        $readyToPlanHtml .= '<table style="width:100%" border="1">';
        $readyToPlanHtml .= '<tr><th></th><th>Snack</th><th>Lunch</th><th>Dessert</th></tr>';
        foreach ($readyToPlanItems as $item) {
            $menuplanner_id = $item['menuplanner_id'];
            $lunch_id = $item['lunch_id'];
            $delivery_date = $item['delivery_date'];
            $day = $item['day'];

            $order = $this->getOrders($item['order_id']);

            foreach ($order->getItems() as $item2) {
               $orderItemId[] = $item2->getProductId();
            }
            if (!in_array(156, $orderItemId)) {
                $isPremiumPlus = false;
            } else {
                $isPremiumPlus = true;
            }

            if($item['is_snack'] == 1){
                $snack = $this->getSnack($delivery_date, $day, $isPremiumPlus);
                $snackId = isset($snack['product_id']) ? $snack['product_id'] : '';
                $snackName = isset($snack['name']) ? $snack['name'] : '';
            } else {
                $snackId = 0;
                $snackName = 'N/A';
            }
            $dessert = $this->getDessert($delivery_date, $day, $isPremiumPlus);
            $dessertId = isset($dessert['product_id']) ? $dessert['product_id'] : '';
            $dessertName = isset($dessert['name']) ? $dessert['name'] : '';
            $lunchListHtml = $this->getLunchListHtml($delivery_date, $delivery_date, $lunch_id, $menuplanner_id, $isPremiumPlus);
            $readyToPlanHtml .= "<tr>";
            $readyToPlanHtml .= "<input name='menuplanner_id[$menuplanner_id]' type='text' value='$menuplanner_id' hidden/>";
            $readyToPlanHtml .= "<td>" . $day . "<br/><span>" . $delivery_date . "</span></td>";
            $readyToPlanHtml .= "<td><input name='snack_id[$menuplanner_id]' type='text' value='$snackId' hidden/>$snackName</td>";
            $readyToPlanHtml .= "<td>$lunchListHtml</td>";
            $readyToPlanHtml .= "<td><input name='dessert_id[$menuplanner_id]' type='text' value='$dessertId' hidden/>$dessertName</td>";
            $readyToPlanHtml .= "</tr>";
        }
        $readyToPlanHtml .= '</table>';
        $readyToPlanHtml .= "<div class='row'><button type='submit'>Save</button></div>";
        $readyToPlanHtml .= "</form > ";
        return $readyToPlanHtml;
    }

    public function getProsessingHtml($prosessingItems)
    {
        $prosessingHtml = '';
        $prosessingHtml .= '<p>Processing week.</p>';
        $prosessingHtml .= '<table style="width:100%" border="1">';
        $prosessingHtml .= '<tr><th></th><th>Snack</th><th>Lunch</th><th>Dessert</th></tr>';
        foreach ($prosessingItems as $item) {
            $menuplanner_id = $item['menuplanner_id'];
            $day = $item['day'];
            $delivery_date = $item['delivery_date'];
            $snack = $item['snack'];
            $lunch = $item['lunch'];
            $dessert = $item['dessert'];
            $prosessingHtml .= "<tr>";
            $prosessingHtml .= "<input name='menuplanner_id' type='text' value='$menuplanner_id' hidden/>";
            $prosessingHtml .= "<td>" . $day . "<br/><span>" . $delivery_date . "</span></td>";
            $prosessingHtml .= "<td>$snack</td>";
            $prosessingHtml .= "<td>$lunch</td>";
            $prosessingHtml .= "<td>$dessert</td>";
            $prosessingHtml .= "</tr>";
        }
        $prosessingHtml .= '</table>';
        return $prosessingHtml;
    }

    public function getSnack($delivery_date, $day, $isPremiumPlus)
    {
        $snackOfTheDay = '';
        $mealsAttr = $this->eavConfig->getAttribute('catalog_product', 'meals_type');

        $daysAttr = $this->eavConfig->getAttribute('catalog_product', 'day_of_snack');
        $dayId = $daysAttr->getSource()->getOptionId($day);
        $snacks = $this->_productCollectionFactory->create();
        if ($isPremiumPlus) {
            $snackId = $mealsAttr->getSource()->getOptionId('snack premium +');
            $snacks
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('meals_type')
                ->addAttributeToFilter('status', 1)
                ->addFieldToFilter('meal_available_to', ['gteq' => $delivery_date])
                ->addFieldToFilter('meal_available_from', ['lteq' => $delivery_date])
                ->addFieldToFilter('meals_type', ['eq' => $snackId])
                ->addFieldToFilter('day_of_snack', ['eq' => $dayId]);
        } else {
            $snackId = $mealsAttr->getSource()->getOptionId('snack');
            $snacks
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('meals_type')
                ->addAttributeToFilter('status', 1)
                ->addFieldToFilter('meal_available_to', ['gteq' => $delivery_date])
                ->addFieldToFilter('meal_available_from', ['lteq' => $delivery_date])
                ->addFieldToFilter('meals_type', ['eq' => $snackId])
                ->addFieldToFilter('day_of_snack', ['eq' => $dayId]);
        }
        foreach ($snacks as $snack) {
            $snackOfTheDay['name'] = $snack->getName();
            $snackOfTheDay['product_id'] = $snack->getId();
            break;
        }

        return $snackOfTheDay;
    }

    public function getLunchListHtml($from, $to, $lunch_id, $menuplanner_id, $isPremiumPlus)
    {
        $html = '';
        $lunchList = [];
        $mealsAttr = $this->eavConfig->getAttribute('catalog_product', 'meals_type');
        $lunchId = $mealsAttr->getSource()->getOptionId('lunch');
        $lunchs = $this->_productCollectionFactory->create();
        if ($isPremiumPlus) {
            $lunchId = $mealsAttr->getSource()->getOptionId('lunch premium +');
            $lunchs
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('meals_type')
                ->addAttributeToFilter('status', 1)
                ->addFieldToFilter('meal_available_to', ['gteq' => $from])
                ->addFieldToFilter('meal_available_from', ['lteq' => $to])
                ->addFieldToFilter('meals_type', ['eq' => $lunchId]);
        } else {
            $lunchId = $mealsAttr->getSource()->getOptionId('lunch');
            $lunchs
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('meals_type')
                ->addAttributeToFilter('status', 1)
                ->addFieldToFilter('meal_available_to', ['gteq' => $from])
                ->addFieldToFilter('meal_available_from', ['lteq' => $to])
                ->addFieldToFilter('meals_type', ['eq' => $lunchId]);
        }

        foreach ($lunchs as $lunch) {
            $lunchList[$lunch->getId()]['name'] = $lunch->getName();
            $lunchList[$lunch->getId()]['product_id'] = $lunch->getId();
        }

        $html .= "<select name='lunch_id[$menuplanner_id]'>";
        foreach ($lunchList as $item) {
            if (!is_null($lunch_id) AND $lunch_id == $item['product_id']) {
                $html .= '<option selected value=' . $item['product_id'] . '>' . $item['name'] . '</option>';
            } else {
                $html .= '<option  value=' . $item['product_id'] . '>' . $item['name'] . '</option>';
            }
        }
        $html .= '</select>';

        return $html;

    }

    public function getDessert($delivery_date, $day, $isPremiumPlus)
    {
        $dessertOfTheDay = '';
        $mealsAttr = $this->eavConfig->getAttribute('catalog_product', 'meals_type');

        $daysAttr = $this->eavConfig->getAttribute('catalog_product', 'day_of_dessert');
        $dayId = $daysAttr->getSource()->getOptionId($day);
        $desserts = $this->_productCollectionFactory->create();
        if ($isPremiumPlus) {
            $dessertId = $mealsAttr->getSource()->getOptionId('dessert premium +');
            $desserts
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('meals_type')
                ->addAttributeToFilter('status', 1)
                ->addFieldToFilter('meal_available_to', ['gteq' => $delivery_date])
                ->addFieldToFilter('meal_available_from', ['lteq' => $delivery_date])
                ->addFieldToFilter('meals_type', ['eq' => $dessertId])
                ->addFieldToFilter('day_of_dessert', ['eq' => $dayId]);
        } else {
            $dessertId = $mealsAttr->getSource()->getOptionId('dessert');
            $desserts
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('meals_type')
                ->addAttributeToFilter('status', 1)
                ->addFieldToFilter('meal_available_to', ['gteq' => $delivery_date])
                ->addFieldToFilter('meal_available_from', ['lteq' => $delivery_date])
                ->addFieldToFilter('meals_type', ['eq' => $dessertId])
                ->addFieldToFilter('day_of_dessert', ['eq' => $dayId]);
        }

        foreach ($desserts as $dessert) {
            $dessertOfTheDay['name'] = $dessert->getName();
            $dessertOfTheDay['product_id'] = $dessert->getId();
            break;
        }

        return $dessertOfTheDay;
    }


    public function getPostUrl()
    {
        return $this->_getUrl("menuplanner/index/save/");
    }

    public function getProsessingItems($child_id, $from, $to)
    {
        $prosessing = [];
        $prosessingItens = $this->menuplanner->create();
        $prosessingItens->addFieldToFilter('child_id', $child_id)
            ->addFieldToFilter('delivery_date', ['gteq' => $from])
            ->addFieldToFilter('delivery_date', ['lteq' => $to]);
        foreach ($prosessingItens as $prosessingIten) {
            $prosessing[$prosessingIten->getMenuplannerId()] = $prosessingIten->getData();
        }
        return $prosessing;
    }

    public function getReadyToPlanItems($child_id, $from, $to)
    {
        $readyToPlan = [];
        $menuItems = $this->menuplanner->create();
        $menuItems->addFieldToFilter('child_id', $child_id)
            ->addFieldToFilter('delivery_date', ['gteq' => $from])
            ->addFieldToFilter('delivery_date', ['lteq' => $to]);
        foreach ($menuItems as $menuItem) {
            $readyToPlan[$menuItem->getMenuplannerId()] = $menuItem->getData();
        }
        return $readyToPlan;
    }

    function getMenuStatus($id, $name){
        $todat = date('Ymd', strtotime('today'));
        $todat = new DateTime($todat);
        $deliveryDate = $this->menuplanner->create()
            ->addFieldToFilter('child_id', ['eq' => $id])
            ->addFieldToFilter('delivery_date', ['gteq' => $todat]);

        $size = $deliveryDate->getSize();
        if($size){
            $from = $deliveryDate->getData()[0]['delivery_date'];
            $to = $deliveryDate->getData()[$size-1]['delivery_date'];
            return "There is a current order for $name, from $from to $to ";
        }else{
            $order_now = '<a href='.$this->checkout_helper->getContinueShoppingUrl().'> Order now</a>';
            return 'There is  no current order for ' . $name .'.'. $order_now;
        }
    }

    public function getOrders($orderId) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('Magento\Sales\Model\Order')->load($orderId);

        return $order;
    }

}