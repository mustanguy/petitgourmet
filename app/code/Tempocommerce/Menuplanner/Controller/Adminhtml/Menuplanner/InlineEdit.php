<?php
namespace Tempocommerce\Menuplanner\Controller\Adminhtml\Menuplanner;
/**
 * Class InlineEdit
 * @package Tempocommerce\Menuplanner\Controller\Adminhtml\Menuplanner
 */
class InlineEdit extends \Magento\Backend\App\Action{


    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_jsonFactory;


    /**
     * @var \Tempocommerce\Menuplanner\Model\MenuplannerFactory
     */
    protected $_menuplannerFactory;


    /**
     * InlineEdit constructor.
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     * @param \Tempocommerce\Menuplanner\Model\MenuplannerFactory $menuplannerFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Tempocommerce\Menuplanner\Model\MenuplannerFactory $menuplannerFactory,
        \Magento\Backend\App\Action\Context $context
    )
    {
        $this->_jsonFactory = $jsonFactory;
        $this->_menuplannerFactory = $menuplannerFactory;
        parent::__construct($context);
    }


    /**
     * @return $this
     */
    public function execute()
    {

        $resultJson = $this->_jsonFactory->create();
        $error = false;
        $messages = [];
        $postData = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postData))) {
            return $resultJson->setData(['messages' => [__('Please correct the data sent.')], 'error' => true,]);
        }
        foreach (array_keys($postData) as $id) {
            $menu = $this->_menuplannerFactory->create()->load($id);
            try {
                $planedData = $postData[$id];
                $menu->addData($planedData);
                $menu->save();
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithRequestId($menu, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithRequestId($menu, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithRequestId(
                    $menu,
                    __($e->getMessage())
                );
                $error = true;
            }
        }
        return $resultJson->setData(['messages' => $messages, 'error' => $error]);
    }


    /**
     * @param \Tempocommerce\Menuplanner\Model\Menuplanner $menu
     * @param $errorText
     * @return string
     */
    protected function getErrorWithRequestId(\Tempocommerce\Menuplanner\Model\Menuplanner $menu, $errorText)
    {
        return '[Request ID: ' . $menu->getMenuplannerId() . '] ' . $errorText;
    }

}