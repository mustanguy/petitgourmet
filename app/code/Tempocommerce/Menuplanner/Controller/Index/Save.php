<?php

namespace Tempocommerce\Menuplanner\Controller\Index;

class Save extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;
    protected $product;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Catalog\Model\ProductFactory $product
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->product = $product;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $menuItems = $this->getRequest()->getParams();
            foreach ($menuItems['menuplanner_id'] as $id) {
                $menuplanner = $this->_objectManager->create('Tempocommerce\Menuplanner\Model\Menuplanner');
                $snack_id = isset($menuItems['snack_id'][$id]) ? $menuItems['snack_id'][$id] : 0;
                $lunch_id = isset($menuItems['lunch_id'][$id]) ? $menuItems['lunch_id'][$id] : 0;
                $dessert_id = isset($menuItems['dessert_id'][$id]) ? $menuItems['dessert_id'][$id] : 0;
                $menuplanner->setMenuplannerId([$id]);
                $menuplanner->setSnackId($snack_id);
                $menuplanner->setLunchId($lunch_id);
                $menuplanner->setDessertId($dessert_id);
                $menuplanner->setSnack($this->getProductName($snack_id));
                $menuplanner->setLunch($this->getProductName($lunch_id));
                $menuplanner->setDessert($this->getProductName($dessert_id));
                $menuplanner->setUpdatedAt(date("d-m-Y H:i:s"));
                $menuplanner->save();
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            var_dump($e->getMessage());
        }
        $this->messageManager->addSuccessMessage(__('Successfully saved.'));
        $this->_redirect($this->_redirect->getRefererUrl());
    }

    public function getProductName($id)
    {
        $product_name = 'default';
        if ($id) {
            $product_name = $this->product->create()->load($id)->getName();
        }

        return $product_name;

    }
}
