<?php
namespace Tempocommerce\Menuplanner\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use DateTime;
use DateInterval;
use DatePeriod;

class Create extends Command
{
    protected $_order;
    protected $eavConfig;
    protected $_objectManager;
    protected $_menuplannerFactory;

    function __construct(
        \Tempocommerce\Menuplanner\Model\MenuplannerFactory $menuplannerFactory,
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\App\Action\Context $context
    )
    {
        $this->_objectManager = $context->getObjectManager();
        $this->_order = $order;
        $this->eavConfig = $eavConfig;
        $this->_menuplannerFactory = $menuplannerFactory;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('petit:menu:create:menu')
            ->addOption(
                'order_id', null, InputOption::VALUE_REQUIRED
            )->setDescription('Create missing menu');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $order_id = $input->getOption('order_id');
        if ($order_id) {
            $order = $this->_order->load($order_id);
            $items = $order->getItems();
            foreach ($items as $item) {
                if (!is_null($item->getParentItemId())) {
                    if (
                        isset($item->getProductOptions()['info_buyRequest']) AND
                        !is_null($item->getProductOptions()['info_buyRequest'])
                    ) {
                        $info = $item->getProductOptions()['info_buyRequest'];
                        $child_id = $info['tempocommerce']['child_id'];
                        $deleveryDates = $this->getDeleveryDate($info);
                        foreach ($deleveryDates as $key => $day) {
                            $menuplanner = $this->_objectManager->create('Tempocommerce\Menuplanner\Model\Menuplanner');
                            $menuplanner->setChildId($child_id);
                            $menuplanner->setDeliveryDate($key);
                            $menuplanner->setOrderId($order_id);
                            $menuplanner->setOrderItemId($item->getId());
                            $menuplanner->setDay($day);
                            $menuplanner->setStatus('pending');
                            $supAtt = $info['super_attribute'];
                            $menuplanner->setIsSnack($this->snackAvailable($supAtt));
                            $menuplanner->setCustomerId($order->getCustomerId());
                            $menuplanner->setCreatedAt(date("d-m-Y H:i:s"));
                            $menuplanner->save();
                        }
                    }
                }
            }
        }
    }

    public function snackAvailable($info)
    {
        $attr = $this->eavConfig->getAttribute('catalog_product', 'meals');
        $attId = $attr->getAttributeId();
        if (isset($info[$attId])) {
            $optionId = $info[$attId];
            $value = $attr->setStoreId(0)->getSource()->getOptionText($optionId);
            if ($value === 'desert_lunch') {
                return 0;
            }else{
                return 1;
            }
        } else {
            return 0;
        }


    }

    public function getDeleveryDate($info_buyRequest)
    {
        $numberOfWeeks = null;
        $deliveryDates = null;
        $startDate = $info_buyRequest['tempocommerce']['calendar_inputField'];
        $options = $info_buyRequest['tempocommerce'];
        $days = $this->getDays($options);
        $subscriptionAttr = $this->eavConfig->getAttribute('catalog_product', 'subscription');
        $subscriptionOps = $subscriptionAttr->setStoreId(0)->getSource()->getAllOptions();
        $subAttId = $subscriptionAttr->getAttributeId();
        $atts = $info_buyRequest['super_attribute'];
        $subOpId = $atts[$subAttId];
        foreach ($subscriptionOps as $subscriptionOp) {
            if ($subscriptionOp['value'] === $subOpId) {
                $numberOfWeeks = $subscriptionOp['label'];
            }
        }
        $deliveryDates = $this->getWeeksInfo($startDate, (int)$numberOfWeeks, $days);

        return $deliveryDates;
    }

    public function getWeeksInfo($startDate, $numberOfWeeks, $days)
    {
        switch ($numberOfWeeks) {
            case 1:
                $daysMargin = 'P7D';
                break;
            case 2:
                $daysMargin = 'P14D';
                break;
            case 3:
                $daysMargin = 'P21D';
                break;
            case 4:
                $daysMargin = 'P28D';
                break;
            case 5:
                $daysMargin = 'P35D';
                break;
            case 6:
                $daysMargin = 'P42D';
                break;
            case 7:
                $daysMargin = 'P49D';
                break;
            case 8:
                $daysMargin = 'P56D';
                break;
            case 9:
                $daysMargin = 'P63D';
                break;
            case 10:
                $daysMargin = 'P70D';
                break;
            case 11:
                $daysMargin = 'P77D';
                break;
            case 12:
                $daysMargin = 'P84D';
                break;
            case 13:
                $daysMargin = 'P91D';
                break;
            case 14:
                $daysMargin = 'P98D';
                break;
            case 15:
                $daysMargin = 'P105D';
                break;
            case 16:
                $daysMargin = 'P112D';
                break;
            default:
                $daysMargin = 'P0D';
        }

        $startDateObj = new DateTime($startDate);
        $endDateObjIni = new DateTime($startDate);
        $endDateObj = $endDateObjIni->add(new DateInterval($daysMargin))->modify('-1 day');
        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($startDateObj, $interval, $endDateObj);


        $deliveryDates = [];
        foreach ($daterange as $date) {
            $day = strtolower(date('D', strtotime($date->format('Ymd'))));
            if (in_array($day, $days)) {
                $deliveryDates[$date->format('Y-m-d')] = $day;
            }

        }

        return $deliveryDates;
    }

    public function getDays($options)
    {
        $days = [];
        $days['sun'] = isset($options['sun']) ? $options['sun'] : null;
        $days['mon'] = isset($options['mon']) ? $options['mon'] : null;
        $days['tue'] = isset($options['tue']) ? $options['tue'] : null;
        $days['wed'] = isset($options['wed']) ? $options['wed'] : null;
        $days['thu'] = isset($options['thu']) ? $options['thu'] : null;
        return array_filter($days);
    }

}