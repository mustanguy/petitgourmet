<?php
namespace Tempocommerce\Menuplanner\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;

class MenuSetup extends Command
{
    protected $_menuplannerFactory;
    protected $eavConfig;
    protected $_productCollectionFactory;
    protected $logger;

    function __construct(
        \Magento\Framework\App\State $state,
        \Tempocommerce\Menuplanner\Model\MenuplannerFactory $menuplannerFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Tempocommerce\Menuplanner\Logger\Logger $logger
    )
    {
        $state->setAreaCode('adminhtml');
        $this->_menuplannerFactory = $menuplannerFactory;
        $this->eavConfig = $eavConfig;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('petit:menu:set:meal');
        $this->setDescription('Default menu setup');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $exDate = ['fri', 'sat', 'sun'];
        $today = date('Y-m-d');
        $today = strtolower(date("D", strtotime($today)));

        if (in_array($today, $exDate)) {
            $nextMove = '6 days';
        } else {
            $nextMove = '-3 days';
        }

        $nextWeekStart = date('Ymd', strtotime('monday next week'));
        $nextWeekEnd = date('Ymd', strtotime('sunday next week'));
        $nextWeekStart = new DateTime($nextWeekStart);
        $nextWeekEnd = new DateTime($nextWeekEnd);
        $nextWeekStartDate = $nextWeekStart->modify($nextMove);
        $nextWeekEndDate = $nextWeekEnd->modify($nextMove);


        $collection = $this->_menuplannerFactory->create()->getCollection()
            ->addFieldToFilter('delivery_date', ['gteq' => $nextWeekStartDate])
            ->addFieldToFilter('delivery_date', ['lteq' => $nextWeekEndDate])
            ->addFieldToFilter('lunch_id', ['eq' => 0]);

        foreach ($collection as $item) {
            try {
                $defaultLunch = $this->getDefaultLunch($item->getDeliveryDate(), $nextWeekStartDate, $nextWeekEndDate);
                if (!empty($defaultLunch)) {
                    $item->setLunchId($defaultLunch['id']);
                    $item->setLunch($defaultLunch['name']);
                    $item->save();
                }
            }catch (\Exception $e){
                $this->logger->info($e->getMessage());
            }
        }

    }

    public function getDefaultLunch($deliveryDate, $start, $end)
    {
        $defaultLunchArray = [];
        $defLunchAttr = $this->eavConfig->getAttribute('catalog_product', 'default_lunch_day');
        $day = strtolower(date("D", strtotime($deliveryDate)));
        $dayId = $defLunchAttr->getSource()->getOptionId($day);
        $mealsAttr = $this->eavConfig->getAttribute('catalog_product', 'meals_type');
        $lunchId = $mealsAttr->getSource()->getOptionId('lunch');
        $defaultLunch = $this->_productCollectionFactory->create();
        $defaultLunch
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('name')
            ->addAttributeToFilter('status', 1)
            ->addFieldToFilter('meal_available_to', ['gteq' => $start])
            ->addFieldToFilter('meal_available_from', ['lteq' => $end])
            ->addFieldToFilter('meals_type', ['eq' => $lunchId])
            ->addFieldToFilter('default_lunch_day', ['finset' => $dayId]);

        foreach ($defaultLunch as $item) {
            $defaultLunchArray['name'] = $item->getName();
            $defaultLunchArray['id'] = $item->getId();
            break;
        }

        return $defaultLunchArray;

    }

}