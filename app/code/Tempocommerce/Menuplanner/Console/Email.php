<?php
namespace Tempocommerce\Menuplanner\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;

class Email extends Command
{
    const XML_PATH_EMAIL_SENDER_NAME = 'petitsetting/petit/sender_name';
    const XML_PATH_EMAIL_SENDER_EMAIL = 'petitsetting/petit/sender_email';
    protected $_menuplannerFactory;
    protected $_customerUrl;
    protected $logger;

    function __construct(
        \Tempocommerce\Menuplanner\Model\MenuplannerFactory $menuplannerFactory,
        \Tempocommerce\Menuplanner\Logger\Logger $logger
    )
    {
        $this->_menuplannerFactory = $menuplannerFactory;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('petit:menu:email-reminder');
        $this->setDescription('Email reminder for menu plan.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $exDate = ['fri', 'sat', 'sun'];
        $today = date('Y-m-d');
        $today = strtolower(date("D", strtotime($today)));

        if (in_array($today, $exDate)) {
            $nextMove = '6 days';
        } else {
            $nextMove = '-3 days';
        }

        $nextWeekStart = date('Ymd', strtotime('monday next week'));
        $nextWeekEnd = date('Ymd', strtotime('sunday next week'));
        $nextWeekStart = new DateTime($nextWeekStart);
        $nextWeekEnd = new DateTime($nextWeekEnd);
        $nextWeekStartDate = $nextWeekStart->modify($nextMove);
        $nextWeekEndDate = $nextWeekEnd->modify($nextMove);


        $collection = $this->_menuplannerFactory->create()->getCollection()
            ->addFieldToFilter('delivery_date', ['gteq' => $nextWeekStartDate])
            ->addFieldToFilter('delivery_date', ['lteq' => $nextWeekEndDate])
            ->addFieldToFilter('lunch_id', ['eq' => 0]);
        $customer_ids = [];
        foreach ($collection as $item) {
            if (!in_array($item->getCustomerId(), $customer_ids)) {
                $customer_ids[] = $item->getCustomerId();
            }
        }
        foreach ($customer_ids as $customer_id){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $helper = $objectManager->get('Tempocommerce\Menuplanner\Helper\Email');
            $customer = $this->getCustomerData($customer_id);
            if(!empty($customer)) {
                $receiverInfo = [
                    'name' => $customer['firstname'],
                    'email' => $customer['email']
                ];
                $senderInfo = [
                    'name' => $helper->getConfigValue(self::XML_PATH_EMAIL_SENDER_NAME,0),
                    'email' => $helper->getConfigValue(self::XML_PATH_EMAIL_SENDER_EMAIL,0)
                ];

                $emailTempVariables = [];

                try {
                    $helper->mailer(
                        $emailTempVariables,
                        $senderInfo,
                        $receiverInfo
                    );
                }catch (\Exception $e){
                    $this->logger->info($receiverInfo['name'].' => '.$receiverInfo['email']);
                    $this->logger->info($e->getMessage());
                }
            }
        }
    }

    public function getCustomerData($customerId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        if($customerObj){
            return $customerObj->getData();
        }else{
            return [];
        }
    }
}