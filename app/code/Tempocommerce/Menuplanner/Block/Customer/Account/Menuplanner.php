<?php


namespace Tempocommerce\Menuplanner\Block\Customer\Account;

class Menuplanner extends \Magento\Framework\View\Element\Template
{

    public function getPostUrl()
    {
        return $this->getUrl('menuplanner/menu/index');
    }
}
