<?php


namespace Tempocommerce\Menuplanner\Model;

use Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface;

class Menuplanner extends \Magento\Framework\Model\AbstractModel implements MenuplannerInterface
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tempocommerce\Menuplanner\Model\ResourceModel\Menuplanner');
    }

    /**
     * Get menuplanner_id
     * @return string
     */
    public function getMenuplannerId()
    {
        return $this->getData(self::MENUPLANNER_ID);
    }

    /**
     * Set menuplanner_id
     * @param string $menuplannerId
     * @return Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     */
    public function setMenuplannerId($menuplannerId)
    {
        return $this->setData(self::MENUPLANNER_ID, $menuplannerId);
    }

    /**
     * Get child_id
     * @return string
     */
    public function getChildId()
    {
        return $this->getData(self::CHILD_ID);
    }

    /**
     * Set child_id
     * @param string $child_id
     * @return Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     */
    public function setChildId($child_id)
    {
        return $this->setData(self::CHILD_ID, $child_id);
    }
	
	/**
     * Get snack
     * @return string
     */
    public function getSnack()
    {
        return $this->getData(self::SNACK);
    }

    /**
     * Set snack
     * @param string $snack
     * @return Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     */
    public function setSnack($snack)
    {
        return $this->setData(self::SNACK, $snack);
    }
	
    /**
     * Get lunch
     * @return string
     */
    public function getLunch()
    {
        return $this->getData(self::LUNCH);
    }

    /**
     * Set lunch
     * @param string $lunch
     * @return Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     */
    public function setLunch($lunch)
    {
        return $this->setData(self::LUNCH, $lunch);
    }

    /**
     * Get dessert
     * @return string
     */
    public function getDessert()
    {
        return $this->getData(self::DESSERT);
    }

    /**
     * Set dessert
     * @param string $dessert
     * @return Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     */
    public function setDessert($dessert)
    {
        return $this->setData(self::DESSERT, $dessert);
    }

    /**
     * Get day_of_week
     * @return string
     */
    public function getDayOfWeek()
    {
        return $this->getData(self::DAY_OF_WEEK);
    }

    /**
     * Set day_of_week
     * @param string $day_of_week
     * @return Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     */
    public function setDayOfWeek($day_of_week)
    {
        return $this->setData(self::DAY_OF_WEEK, $day_of_week);
    }	
}
