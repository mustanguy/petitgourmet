<?php


namespace Tempocommerce\Menuplanner\Model\ResourceModel\Menuplanner;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Tempocommerce\Menuplanner\Model\Menuplanner',
            'Tempocommerce\Menuplanner\Model\ResourceModel\Menuplanner'
        );
    }

    protected function _initSelect()
    {

        parent::_initSelect();
        $this->getSelect()->joinLeft(
            ['childrenTable' => $this->getTable('tempocommerce_children')],
            'main_table.child_id = childrenTable.children_id',
            ['first_name','last_name','birth_date','allergies','nursery','class_num']
        )->joinLeft(
            ['customerTable' => $this->getTable('customer_entity')],
            'main_table.customer_id = customerTable.entity_id',
            ['firstname','lastname','email']
        )->joinLeft(
            ['customerAddressTable' => $this->getTable('customer_address_entity')],
            'main_table.customer_id = customerAddressTable.parent_id',
            ['telephone']
        )->joinLeft(
            ['nurseryTable' => $this->getTable('tempocommerce_nursery')],
            'childrenTable.nursery = nurseryTable.nursery_id',
            ['nusery_name' => 'name']
        )->group('menuplanner_id');
    }
}
