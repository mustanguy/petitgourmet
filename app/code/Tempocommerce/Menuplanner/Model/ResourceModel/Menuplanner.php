<?php


namespace Tempocommerce\Menuplanner\Model\ResourceModel;

class Menuplanner extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('tempocommerce_children_menuplanner', 'menuplanner_id');
    }
}
