<?php

namespace Tempocommerce\Menuplanner\Model;

use Tempocommerce\Menuplanner\Api\MenuplannerRepositoryInterface;
use Tempocommerce\Menuplanner\Api\Data\MenuplannerSearchResultsInterfaceFactory;
use Tempocommerce\Menuplanner\Api\Data\MenuplannerInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Tempocommerce\Menuplanner\Model\ResourceModel\Menuplanner as ResourceMenuplanner;
use Tempocommerce\Menuplanner\Model\ResourceModel\Menuplanner\CollectionFactory as MenuplannerCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class MenuplannerRepository implements MenuplannerRepositoryInterface
{

    protected $resource;

    protected $menuplannerFactory;

    protected $menuplannerCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataMenuplannerFactory;

    private $storeManager;


    /**
     * @param ResourceMenuplanner $resource
     * @param MenuplannerFactory $menuplannerFactory
     * @param MenuplannerInterfaceFactory $dataMenuplannerFactory
     * @param MenuplannerCollectionFactory $menuplannerCollectionFactory
     * @param MenuplannerSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceMenuplanner $resource,
        MenuplannerFactory $menuplannerFactory,
        MenuplannerInterfaceFactory $dataMenuplannerFactory,
        MenuplannerCollectionFactory $menuplannerCollectionFactory,
        MenuplannerSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->menuplannerFactory = $menuplannerFactory;
        $this->menuplannerCollectionFactory = $menuplannerCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataMenuplannerFactory = $dataMenuplannerFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface $menuplanner
    ) {
        /* if (empty($menuplanner->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $menuplanner->setStoreId($storeId);
        } */
        try {
            $menuplanner->getResource()->save($menuplanner);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the menuplanner: %1',
                $exception->getMessage()
            ));
        }
        return $menuplanner;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($menuplannerId)
    {
        $menuplanner = $this->menuplannerFactory->create();
        $menuplanner->getResource()->load($menuplanner, $menuplannerId);
        if (!$menuplanner->getId()) {
            throw new NoSuchEntityException(__('Menuplanner with id "%1" does not exist.', $menuplannerId));
        }
        return $menuplanner;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->menuplannerCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface $menuplanner
    ) {
        try {
            $menuplanner->getResource()->delete($menuplanner);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Menuplanner: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($menuplannerId)
    {
        return $this->delete($this->getById($menuplannerId));
    }
}
