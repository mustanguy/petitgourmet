<?php

namespace Tempocommerce\Menuplanner\Api\Data;

interface MenuplannerSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get Menuplanner list.
     * @return \Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface[]
     */

    public function getItems();

    /**
     * Set entity_id list.
     * @param \Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface[] $items
     * @return $this
     */

    public function setItems(array $items);
}
