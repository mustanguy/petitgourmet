<?php


namespace Tempocommerce\Menuplanner\Api\Data;

interface MenuplannerInterface
{

    const MENUPLANNER_ID = 'menuplanner_id';
    const CHILD_ID = 'child_id';
    const SNACK = 'snack';
    const LUNCH = 'lunch';
    const DESSERT = 'dessert';
    const DAY_OF_WEEK = 'day_of_week';


    /**
     * Get menuplanner_id
     * @return string|null
     */
    
    public function getMenuplannerId();

    /**
     * Set menuplanner_id
     * @param string $menuplanner_id
     * @return Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     */
    
    public function setMenuplannerId($menuplannerId);

    /**
     * Get child_id
     * @return string|null
     */
    
    public function getChildId();

    /**
     * Set child_id
     * @param string $child_id
     * @return Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     */
    
    public function setChildId($child_id);
	
	/**
     * Get snack
     * @return string|null
     */
    
    public function getSnack();

    /**
     * Set snack
     * @param string $snack
     * @return Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     */
    
    public function setSnack($snack);
	
	/**
     * Get lunch
     * @return string|null
     */
    
    public function getLunch();

    /**
     * Set lunch
     * @param string $lunch
     * @return Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     */
    
    public function setLunch($lunch);
	
	/**
     * Get dessert
     * @return string|null
     */
    
    public function getDessert();

    /**
     * Set dessert
     * @param string $dessert
     * @return Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     */
    
    public function setDessert($dessert);
	
	/**
     * Get day_of_week
     * @return string|null
     */
    
    public function getDayOfWeek();

    /**
     * Set day_of_week
     * @param string $day_of_week
     * @return Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     */
    
    public function setDayOfWeek($day_of_week);
}
