<?php

namespace Tempocommerce\Menuplanner\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface MenuplannerRepositoryInterface
{


    /**
     * Save Menuplanner
     * @param \Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface $menuplanner
     * @return \Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function save(
        \Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface $menuplanner
    );

    /**
     * Retrieve Menuplanner
     * @param string $menuplannerId
     * @return \Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function getById($menuplannerId);

    /**
     * Retrieve Menuplanner matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Tempocommerce\Menuplanner\Api\Data\MenuplannerSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Menuplanner
     * @param \Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface $menuplanner
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function delete(
        \Tempocommerce\Menuplanner\Api\Data\MenuplannerInterface $menuplanner
    );

    /**
     * Delete Menuplanner by ID
     * @param string $menuplannerId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function deleteById($menuplannerId);
}
