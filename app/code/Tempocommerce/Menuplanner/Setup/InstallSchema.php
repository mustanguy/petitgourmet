<?php


namespace Tempocommerce\Menuplanner\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $table_tempocommerce_children_menuplanner = $setup->getConnection()->newTable($setup->getTable('tempocommerce_children_menuplanner'));

        
        $table_tempocommerce_children_menuplanner->addColumn(
            'menuplanner_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,),
            'Menuplanner ID'
        );
        

        
        $table_tempocommerce_children_menuplanner->addColumn(
            'child_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Child Id'
        );
		
		$table_tempocommerce_children_menuplanner->addColumn(
            'snack',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
			[],
            'Snack'
		);
		
		$table_tempocommerce_children_menuplanner->addColumn(
            'lunch',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'Lunch'
		);
		
		$table_tempocommerce_children_menuplanner->addColumn(
            'dessert',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
			[],
            'Dessert'
		);
		
		$table_tempocommerce_children_menuplanner->addColumn(
            'day_of_week',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'Day of the week'
		);
        
        $setup->getConnection()->createTable($table_tempocommerce_children_menuplanner);

        $setup->endSetup();
    }
}
