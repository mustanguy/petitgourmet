<?php

namespace Tempocommerce\Menuplanner\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $tableName = $setup->getTable('tempocommerce_children_menuplanner');
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName, 'order_id',
                    ['type' => Table::TYPE_BIGINT, 'nullable' => false, 'default' => 0,
                        'comment' => 'Order Id']
                );

                $connection->addColumn(
                    $tableName, 'order_item_id',
                    ['type' => Table::TYPE_BIGINT, 'nullable' => false, 'default' => 0,
                        'comment' => 'Item Id Of Order']
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->changeColumn(
                    $tableName,
                    'day_of_week',
                    'delivery_date',
                    Table::TYPE_DATE,
                    false,
                    null
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName, 'customer_id',
                    ['type' => Table::TYPE_BIGINT, 'nullable' => false, 'default' => 0,
                        'comment' => 'Customer Id']
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName, 'created_at',
                    ['type' => Table::TYPE_DATETIME, 'nullable' => false, 'default' => 0,
                        'comment' => 'Created At']
                );
                $connection->addColumn(
                    $tableName, 'updated_at',
                    ['type' => Table::TYPE_DATETIME, 'nullable' => false, 'default' => 0,
                        'comment' => 'Updated At']
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName, 'snack_id',
                    ['type' => Table::TYPE_BIGINT, 'nullable' => false, 'default' => 0,
                        'comment' => 'Snack Product Id']
                );
                $connection->addColumn(
                    $tableName, 'lunch_id',
                    ['type' => Table::TYPE_BIGINT, 'nullable' => false, 'default' => 0,
                        'comment' => 'Snack Product Id']
                );
                $connection->addColumn(
                    $tableName, 'dessert_id',
                    ['type' => Table::TYPE_BIGINT, 'nullable' => false, 'default' => 0,
                        'comment' => 'Dessert Product Id']
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.6') < 0) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName, 'day',
                    ['type' => Table::TYPE_TEXT, 'nullable' => true,
                        'comment' => 'Week day']
                );
                $connection->addColumn(
                    $tableName, 'status',
                    ['type' => Table::TYPE_TEXT, 'nullable' => true,
                        'comment' => 'Status']
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.7') < 0) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName, 'is_snack',
                    ['type' => Table::TYPE_INTEGER, 'default' => 0,
                        'comment' => 'Snacks']
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.8') < 0) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName, 'is_premium',
                    ['type' => Table::TYPE_INTEGER, 'default' => 0,
                        'comment' => 'Premium']
                );
            }
        }

        $setup->endSetup();
    }

}
