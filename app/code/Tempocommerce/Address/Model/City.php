<?php
/**
 * This model is for handle 2 tables:
 * directory_country_city - base
 * directory_country_city_name - localised
 * with cities to display cities as select in shipping address
 * Like standard Magento region
 *
 */
namespace Tempocommerce\Address\Model;

class City extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tempocommerce\Address\Model\ResourceModel\City');
    }

    /**
     * Retrieve city name
     *
     * If name is no declared, then default_name is used
     *
     * @return string
     */
    public function getName()
    {
        $name = $this->getData('name');
        if ($name === null) {
            $name = $this->getData('default_name');
        }
        return $name;
    }

    /**
     * Load city by name
     *
     * @param string $name
     * @param string $countryId
     * @return $this
     */
    public function loadByName($name, $countryId)
    {
        $this->_getResource()->loadByName($this, $name, $countryId);
        return $this;
    }
}
