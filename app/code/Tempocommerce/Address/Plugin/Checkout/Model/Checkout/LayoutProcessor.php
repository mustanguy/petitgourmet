<?php
/**
 * Plugin to add our uicomponent select cities to layout displayed in shipping address
 */

namespace Tempocommerce\Address\Plugin\Checkout\Model\Checkout;

class LayoutProcessor
{
    private $citiesFactory = null;

    public function __construct(\Tempocommerce\Address\Model\ResourceModel\City\CollectionFactory $citiesFactory)
    {
        $this->citiesFactory = $citiesFactory;
    }

    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
    \Magento\Checkout\Block\Checkout\LayoutProcessor $subject, array $jsLayout
    )
    {
        $cities              = $this->citiesFactory->create()->toOptionArray();
        //array of arrays value title country_id label despite [0] which is label type of Magento\Framework\Phrase
        // where text is "Please select a city."
        $customAttributeCode = 'select_city';
        $customField         = [
            'component' => 'Tempocommerce_Address/js/form/element/city',
            'config' => [
                // customScope is used to group elements within a single form (e.g. they can be validated separately)
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select',
                'id' => 'select-city',
            ],
            'dataScope' => 'shippingAddress.custom_attributes'.'.'.$customAttributeCode,
            'label' => 'City',
            'provider' => 'checkoutProvider',
            //city as input type text has sortOrder 80
            'sortOrder' => 81,
            'id' => 'select-city',
            'validation' => [
                'required-entry' => true
            ],
            /* 'options' => [
              [
              'value' => '',
              'label' => 'Please Select',
              ],
              [
              'value' => '1',
              'label' => 'First Option',
              ]
              ], */
            'options' => $cities,
            /*TODO I don't know why, target is empty
            'filterBy' => [
                'target' => '${ $.provider }:${ $.parentScope }.country_id',
                'field' => 'country_id',
            ],
             */
            'customEntry' => null,
            'visible' => true,
        ];

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']
            ['shipping-address-fieldset']['children'][$customAttributeCode] = $customField;

        return $jsLayout;
    }
}