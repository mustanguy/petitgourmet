<?php
/**
 * Plugin to add country information to countries list
 */

namespace Tempocommerce\Address\Plugin\Country;

class Collection
{
    private $helper = null;
    public function __construct(\Tempocommerce\Address\Helper\Data $helper)
    {
        $this->helper = $helper;
    }
    /**
     * Add to options 2 rows, used at frontend in shipping addres
     * @param \Magento\Directory\Model\ResourceModel\Country\Collection $subject
     * @param array $options with indices: value (country iso), label (country name), eventually is_region_visible and so
     * @return array
     */
    public function afterToOptionArray(\Magento\Directory\Model\ResourceModel\Country\Collection $subject,
                                       $options)
    {
        foreach ($options as $index => $option) {
            if ($this->helper->isZipCodeHidden($option['value'])) {
                $options[$index]['is_zipcode_hidden'] = true;
            }
            if ($this->helper->ifDisplayCitiesAsList($option['value'])) {
                $options[$index]['cities_as_list'] = true;
            }
        }
        return $options;
    }
}