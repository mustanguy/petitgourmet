/**
 * This element extends Magento_Ui/js/form/element/select
 * Is for displaying cities as select list in shipping address
 * It reasemble magento region.js
 * TODO 
 * 2. bind to select country, if right country, fill control 
 * and hide control input cities
 * 3. on change fill hidden control input cities
 * 4. required! validate
 */
define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select'
],
        function (_, registry, select) {
            'use strict';
            /**
             * Return select value if label exists as text
             * @param {type} data - select
             * @param {type} text to check if exists in select
             * @returns int index of select
             */
            function getSelectValueByLabel(data, text) {
                var value;
                if (text) {
                    data.some(function (node) {
                        if (node.label === text.trim()) {
                            value = node.value; 
                            return;
                        }
                    });
                }
                return value;
            }
            ;
            return select.extend({
                defaults: {
                    imports: {
                        update: '${ $.parentName }.country_id:value'
                    },
                    listens: {
                        'value': 'onSelectChange'
                    }
                },
                /**
                 * called when county is changed
                 * @param {String} value
                 * 
                 */
                update: function (value) {
                    var country = registry.get(this.parentName + '.' + 'country_id'),
                            options = country.indexedOptions,
                            option;

                    if (!value) {
                        return;
                    }

                    option = options[value];

                    var city = registry.get(this.parentName + '.' + 'city');
                    if (option['cities_as_list']) {
                        //hide city as input text    
                        city.visible(false);
                        city.error(false);
                        city.validation = _.omit(city.validation, 'required-entry');
                        //show city as select
                        this.filter(value, 'country_id');
                        //if city text exists in select, set select to this this value
                        //and leave city text, else clean city text
                        var val = getSelectValueByLabel(this.options, city.value());
                        if (val) {
                            this.value(val);
                        } else {
                            city.value('');
                        }
                        this.setVisible(true);
                        this.validation['required-entry'] = true;
                    } else {
                        //show city as input text    
                        city.visible(true);
                        city.validation['required-entry'] = true;
                        //hide city as select
                        this.setVisible(false);
                        this.error(false);
                        this.validation = _.omit(this.validation, 'required-entry');
                    }
                },
                /**
                 * called when this select is changed
                 * we set value for (hidden) input text city
                 * @param {type} value
                 * @returns {undefined}
                 */
                onSelectChange: function (value) {
                    //for the first time if changing from displaying city as text, 
                    //value is undefined
                    if (value) {
                        var city = registry.get(this.parentName + '.' + 'city');
                        city.value(this.indexedOptions[value].label);
                    }
                },
                /**
                 * I don't know, not works with initial
                 * 
                 * Filters 'initialOptions' property by 'field' and 'value' passed,
                 * calls 'setOptions' passing the result to it
                 * 
                 * see Magento_Ui/js/form/element/select.js
                 *
                 * @param {*} value
                 * @param {String} field
                 */
                filter: function (value, field) {
                    this._super(value, field);
                }
            });
        }
);
