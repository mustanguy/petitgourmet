/**
 * This component checks if post-code for selected country should be hidden 
 * Only optional zipcode can be hidded
 * It replaces Magento_Ui/js/form/element/post-code
 */

define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/abstract'
], function (_, registry, Abstract) {
    'use strict';

    return Abstract.extend({
        defaults: {
            imports: {
                update: '${ $.parentName }.country_id:value'
            }
        },

        /**
         * @param {String} value
         */
        update: function (value) {
            var country = registry.get(this.parentName + '.' + 'country_id'),
                options = country.indexedOptions,
                option;

            if (!value) {
                return;
            }

            option = options[value];

            if (option['is_zipcode_hidden'] && option['is_zipcode_optional']) {
                //we must hide this control and ensure that can be saved
                //ie remove validation
                this.error(false);
                this.validation = _.omit(this.validation, 'required-entry');
                this.required(false);
                this.visible(false);
            } else {
                //because it maybe hidden for other country, so must show now
                this.visible(true);
                //this is standard fragment from magento post-code
                if (option['is_zipcode_optional']) {
                    this.error(false);
                    this.validation = _.omit(this.validation, 'required-entry');
                } else {
                    this.validation['required-entry'] = true;
                }

                this.required(!option['is_zipcode_optional']);
            }
        }
    });
});
