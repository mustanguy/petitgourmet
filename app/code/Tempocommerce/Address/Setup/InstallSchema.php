<?php

namespace Tempocommerce\Address\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup,
                            ModuleContextInterface $context)
    {
        $setup->startSetup();
        /**
         * Create table 'directory_country_city'
         */
        $table = $setup->getConnection()->newTable(
            $setup->getTable('directory_country_city')
        )->addColumn(
            'city_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'City Id'
        )->addColumn(
            'country_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            4,
            ['nullable' => false, 'default' => '0'],
            'Country Id in ISO-2'
        )->addColumn(
            'default_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'City Name'
        )->addIndex(
            $setup->getIdxName('directory_country_city', ['country_id']),
            ['country_id']
        )->setComment(
            'Directory Country City'
        );
        $setup->getConnection()->createTable($table);

        /**
         * Create table 'directory_country_city_name'
         */
        $table = $setup->getConnection()->newTable(
            $setup->getTable('directory_country_city_name')
        )->addColumn(
            'locale',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            8,
            ['nullable' => false, 'primary' => true, 'default' => false],
            'Locale'
        )->addColumn(
            'city_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true, 'default' => '0'],
            'City Id'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => null],
            'City Name'
        )->addIndex(
            $setup->getIdxName('directory_country_city_name', ['city_id']),
            ['city_id']
        )->addForeignKey(
            $setup->getFkName(
                'directory_country_city_name',
                'city_id',
                'directory_country_city',
                'city_id'
            ),
            'city_id',
            $setup->getTable('directory_country_city'),
            'city_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Directory Country City Name'
        );
        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}