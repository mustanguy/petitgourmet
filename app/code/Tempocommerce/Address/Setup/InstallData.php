<?php

namespace Tempocommerce\Address\Setup;

use Magento\Directory\Helper\Data;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    private $helper;

    public function __construct(\Tempocommerce\Address\Helper\Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        
        /**
         * Fill table directory/country_city
         * Fill table directory/country_city_name for en_US locale
         */
        $data = [
            ['AE', 'Dubai'],
            ['AE', 'Abu Dhabi'],
            ['AE', 'Sharjah'],
            ['AE', 'Al Ain'],
            ['AE', 'Fujairah'],
            ['AE', 'Ras Al Khaimah'],
            ['AE', 'Ajman'],
            ['AE', 'Western Region'],
            ['AE', 'Umm Al Quwain'],

            ['BH', 'Riffa'],
            ['BH', 'Durrat'],
            ['BH', 'Isa Town'],
            ['BH', 'Muharraq'],
            ['BH', 'Sitra'],
            ['BH', 'Manama'],
            ['BH', 'Budaiya'],
            ['BH', 'Madinat Hamad'],

            ['SA', 'Riyadh'],
            ['SA', 'Dammam'],
            ['SA', 'Jeddah'],
            ['SA', 'Al Jubail'],
            ['SA', 'Mecca'],
            ['SA', 'Taif'],
            ['SA', 'Al Khobar'],
            ['SA', 'Al Qatif'],
            ['SA', 'Buraydah'],
            ['SA', 'Unayzah'],
            
            ['EG', 'Alexandria'],
            ['EG', 'Cairo'],
            ['EG', 'Canal'],
            ['EG', 'Delta'],
            ['EG', 'Giza'],
            ['EG', 'Matrouh'],
            ['EG', 'Red sea'],
            ['EG', 'Sinai'],
            ['EG', 'Upper Egypt'],

        ];

        foreach ($data as $row) {
            $bind = ['country_id' => $row[0], 'default_name' => $row[1]];
            $setup->getConnection()->insert($setup->getTable('directory_country_city'), $bind);
            $cityId = $setup->getConnection()->lastInsertId($setup->getTable('directory_country_city'));

            $bind = ['locale' => 'en_US', 'city_id' => $cityId, 'name' => $row[1]];
            $setup->getConnection()->insert($setup->getTable('directory_country_city_name'), $bind);
        }
    }
}
