<?php
namespace Tempocommerce\Address\Setup;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.1.0', '<')) {
            //delete Western Region
            $sql = " delete c, cn from " . $setup->getTable('directory_country_city') . " as c "
                . " inner join " . $setup->getTable('directory_country_city_name') . " as cn "
                . " on c.city_id = cn.city_id "
                . " where c.default_name = 'Western Region' ";
            $setup->getConnection()->query($sql);
        }
        $setup->endSetup();
    }
}
