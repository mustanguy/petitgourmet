<?php

namespace Tempocommerce\Address\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * ISO2 country codes which have Zip/Postal hidden
     *
     * @var array
     */
    private $hiddenZipCountries = null;

    /**
     * ISO2 country codes which have cities displayed as list
     *
     * @var array
     */
    private $citiesAsListCountries = null;

    /**
     * Config value that lists ISO2 country codes which have hidden Zip/Postal
     */
    const HIDDEN_ZIP_COUNTRIES_CONFIG_PATH = 'general/country/hidden_zip_countries';

    /**
     * Config value that lists ISO2 country codes which have cities as list
     */
    const CITIES_LIST_COUNTRIES_CONFIG_PATH = 'general/country/cities_as_list_countries';

    public function __construct(
        \Magento\Framework\App\Helper\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * Check whether zip code is hidden for specified country code
     *
     * @param string $countryCode
     * @return boolean
     */
    public function isZipCodeHidden($countryCode)
    {
        return in_array($countryCode, $this->getCountriesWithHiddenZip());
    }

    public function ifDisplayCitiesAsList($countryCode)
    {
        return in_array($countryCode, $this->getCountriesWithCitiesAsList());
    }

    private function getCountriesWithHiddenZip()
    {
        if (is_null($this->hiddenZipCountries)) {
            try {
                $value                    = trim(
                    $this->scopeConfig->getValue(
                        self::HIDDEN_ZIP_COUNTRIES_CONFIG_PATH,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    )
                );
                $this->hiddenZipCountries = preg_split('/\,/', $value, 0,
                    PREG_SPLIT_NO_EMPTY);
            } catch (\Exception $e) {
                $d = $e;
            } catch (\Throwable $t) {
                $d = $t;
            }
        }
        return $this->hiddenZipCountries;
    }

    private function getCountriesWithCitiesAsList()
    {
        if (is_null($this->citiesAsListCountries)) {
            try {
                $value                       = trim(
                    $this->scopeConfig->getValue(
                        self::CITIES_LIST_COUNTRIES_CONFIG_PATH,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    )
                );
                $this->citiesAsListCountries = preg_split('/\,/', $value, 0,
                    PREG_SPLIT_NO_EMPTY);
            } catch (\Exception $e) {
                $d = $e;
            } catch (\Throwable $t) {
                $d = $t;
            }
        }
        return $this->citiesAsListCountries;
    }
}