<?php


namespace Tempocommerce\Nursery\Api\Data;

interface NurserySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get Nursery list.
     * @return \Tempocommerce\Nursery\Api\Data\NurseryInterface[]
     */
    
    public function getItems();

    /**
     * Set name list.
     * @param \Tempocommerce\Nursery\Api\Data\NurseryInterface[] $items
     * @return $this
     */
    
    public function setItems(array $items);
}
