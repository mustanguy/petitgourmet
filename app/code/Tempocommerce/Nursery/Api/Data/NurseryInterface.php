<?php


namespace Tempocommerce\Nursery\Api\Data;

interface NurseryInterface
{

    const NAME = 'name';
    const PHONE = 'phone';
    const NURSERY_ID = 'nursery_id';


    /**
     * Get nursery_id
     * @return string|null
     */
    
    public function getNurseryId();

    /**
     * Set nursery_id
     * @param string $nursery_id
     * @return \Tempocommerce\Nursery\Api\Data\NurseryInterface
     */
    
    public function setNurseryId($nurseryId);

    /**
     * Get name
     * @return string|null
     */
    
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \Tempocommerce\Nursery\Api\Data\NurseryInterface
     */
    
    public function setName($name);

    /**
     * Get phone
     * @return string|null
     */
    
    public function getPhone();

    /**
     * Set phone
     * @param string $phone
     * @return \Tempocommerce\Nursery\Api\Data\NurseryInterface
     */
    
    public function setPhone($phone);
}
