<?php


namespace Tempocommerce\Nursery\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface NurseryRepositoryInterface
{


    /**
     * Save Nursery
     * @param \Tempocommerce\Nursery\Api\Data\NurseryInterface $nursery
     * @return \Tempocommerce\Nursery\Api\Data\NurseryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function save(
        \Tempocommerce\Nursery\Api\Data\NurseryInterface $nursery
    );

    /**
     * Retrieve Nursery
     * @param string $nurseryId
     * @return \Tempocommerce\Nursery\Api\Data\NurseryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function getById($nurseryId);

    /**
     * Retrieve Nursery matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Tempocommerce\Nursery\Api\Data\NurserySearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Nursery
     * @param \Tempocommerce\Nursery\Api\Data\NurseryInterface $nursery
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function delete(
        \Tempocommerce\Nursery\Api\Data\NurseryInterface $nursery
    );

    /**
     * Delete Nursery by ID
     * @param string $nurseryId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    
    public function deleteById($nurseryId);
}
