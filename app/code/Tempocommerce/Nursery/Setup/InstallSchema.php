<?php


namespace Tempocommerce\Nursery\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $table_tempocommerce_nursery = $setup->getConnection()->newTable($setup->getTable('tempocommerce_nursery'));

        
        $table_tempocommerce_nursery->addColumn(
            'nursery_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,),
            'Entity ID'
        );
        

        
        $table_tempocommerce_nursery->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'Nursery Name'
        );
        

        
        $table_tempocommerce_nursery->addColumn(
            'phone',
            \Magento\Framework\DB\Ddl\Table::TYPE_NUMERIC,
            null,
            [],
            'Nursery Phone number'
        );
        

        $setup->getConnection()->createTable($table_tempocommerce_nursery);

        $setup->endSetup();
    }
}
