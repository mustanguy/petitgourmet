<?php


namespace Tempocommerce\Nursery\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        $tableName = $setup->getTable('tempocommerce_nursery');
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->changeColumn(
                    $tableName,
                    'phone',
                    'phone',
                    Table::TYPE_TEXT,
                    false,
                    null
                );

                $connection->addColumn(
                    $tableName,
                    'address',
                    [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'comment' => 'Address Of Nursery']
                );

            }
        }
        $setup->endSetup();
    }
}
