<?php


namespace Tempocommerce\Nursery\Controller\Adminhtml\Nursery;

class Delete extends \Tempocommerce\Nursery\Controller\Adminhtml\Nursery
{

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('nursery_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create('Tempocommerce\Nursery\Model\Nursery');
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Nursery.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['nursery_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Nursery to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
