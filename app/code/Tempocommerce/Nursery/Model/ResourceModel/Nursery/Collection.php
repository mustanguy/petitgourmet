<?php


namespace Tempocommerce\Nursery\Model\ResourceModel\Nursery;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Tempocommerce\Nursery\Model\Nursery',
            'Tempocommerce\Nursery\Model\ResourceModel\Nursery'
        );
    }

    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
        $countSelect->reset(\Zend_Db_Select::GROUP);
        return $countSelect;
    }
}
