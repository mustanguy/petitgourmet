<?php


namespace Tempocommerce\Nursery\Model\ResourceModel;

class Nursery extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('tempocommerce_nursery', 'nursery_id');
    }
}
