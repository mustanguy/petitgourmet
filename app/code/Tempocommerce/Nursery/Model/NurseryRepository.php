<?php


namespace Tempocommerce\Nursery\Model;

use Tempocommerce\Nursery\Api\Data\NurseryInterfaceFactory;
use Tempocommerce\Nursery\Api\Data\NurserySearchResultsInterfaceFactory;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Reflection\DataObjectProcessor;
use Tempocommerce\Nursery\Model\ResourceModel\Nursery as ResourceNursery;
use Tempocommerce\Nursery\Api\NurseryRepositoryInterface;
use Tempocommerce\Nursery\Model\ResourceModel\Nursery\CollectionFactory as NurseryCollectionFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;

class NurseryRepository implements NurseryRepositoryInterface
{

    protected $dataObjectProcessor;

    protected $NurseryFactory;

    protected $searchResultsFactory;

    private $storeManager;

    protected $dataObjectHelper;

    protected $resource;

    protected $NurseryCollectionFactory;

    protected $dataNurseryFactory;


    /**
     * @param ResourceNursery $resource
     * @param NurseryFactory $nurseryFactory
     * @param NurseryInterfaceFactory $dataNurseryFactory
     * @param NurseryCollectionFactory $nurseryCollectionFactory
     * @param NurserySearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceNursery $resource,
        NurseryFactory $nurseryFactory,
        NurseryInterfaceFactory $dataNurseryFactory,
        NurseryCollectionFactory $nurseryCollectionFactory,
        NurserySearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->nurseryFactory = $nurseryFactory;
        $this->nurseryCollectionFactory = $nurseryCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataNurseryFactory = $dataNurseryFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Tempocommerce\Nursery\Api\Data\NurseryInterface $nursery
    ) {
        /* if (empty($nursery->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $nursery->setStoreId($storeId);
        } */
        try {
            $nursery->getResource()->save($nursery);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the nursery: %1',
                $exception->getMessage()
            ));
        }
        return $nursery;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($nurseryId)
    {
        $nursery = $this->nurseryFactory->create();
        $nursery->getResource()->load($nursery, $nurseryId);
        if (!$nursery->getId()) {
            throw new NoSuchEntityException(__('Nursery with id "%1" does not exist.', $nurseryId));
        }
        return $nursery;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->nurseryCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Tempocommerce\Nursery\Api\Data\NurseryInterface $nursery
    ) {
        try {
            $nursery->getResource()->delete($nursery);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Nursery: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($nurseryId)
    {
        return $this->delete($this->getById($nurseryId));
    }
}
