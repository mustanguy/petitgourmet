<?php


namespace Tempocommerce\Nursery\Model;

use Tempocommerce\Nursery\Api\Data\NurseryInterface;

class Nursery extends \Magento\Framework\Model\AbstractModel implements NurseryInterface
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tempocommerce\Nursery\Model\ResourceModel\Nursery');
    }

    /**
     * Get nursery_id
     * @return string
     */
    public function getNurseryId()
    {
        return $this->getData(self::NURSERY_ID);
    }

    /**
     * Set nursery_id
     * @param string $nurseryId
     * @return \Tempocommerce\Nursery\Api\Data\NurseryInterface
     */
    public function setNurseryId($nurseryId)
    {
        return $this->setData(self::NURSERY_ID, $nurseryId);
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \Tempocommerce\Nursery\Api\Data\NurseryInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get phone
     * @return string
     */
    public function getPhone()
    {
        return $this->getData(self::PHONE);
    }

    /**
     * Set phone
     * @param string $phone
     * @return \Tempocommerce\Nursery\Api\Data\NurseryInterface
     */
    public function setPhone($phone)
    {
        return $this->setData(self::PHONE, $phone);
    }
}
