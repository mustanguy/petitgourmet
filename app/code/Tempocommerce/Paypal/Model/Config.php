<?php
namespace Tempocommerce\Paypal\Model;

class Config extends \Magento\Paypal\Model\Config
{

    public function isCurrencyCodeSupported($code)
    {
        if ($code == 'AED') {
            return true;
        } else {
            return parent::isCurrencyCodeSupported($code);
        }
    }
}