<?php

namespace Tempocommerce\Paypal\Block\Express\InContext\Minicart;

class Button extends \Magento\Paypal\Block\Express\InContext\Minicart\Button{

    private function isVisibleOnCart()
    {
        return false;
    }

    protected function shouldRender()
    {
        return parent::shouldRender() && $this->isVisibleOnCart();
    }
}