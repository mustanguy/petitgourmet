<?php
namespace Tempocommerce\Paypal\Helper;

use Magento\Framework\App\Helper\Context;

/**
 * Class Data
 * @package Tempocommerce\Paypal\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * Data constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param Context $context
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        Context $context
    )
    {
        $this->_storeManager = $storeManager;
        $this->priceCurrency = $priceCurrency;
        parent::__construct($context);
    }

    /**
     * @param int $amount
     * @param null $store
     * @param null $currency
     * @return float
     */
    function convert($amount, $store = null, $currency = null){
        return $this->priceCurrency->round($this->priceCurrency->convert($amount, $store, 'USD'));
    }
}