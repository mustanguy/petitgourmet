<?php
namespace Tempocommerce\Paypal\Plugin\Model\Api;

use Magento\Payment\Model\Cart;
class Nvp
{

    protected $helper;

    protected $_lineItemTotalExportMap = [
        Cart::AMOUNT_SUBTOTAL => 'ITEMAMT',
        Cart::AMOUNT_TAX => 'TAXAMT',
        Cart::AMOUNT_SHIPPING => 'SHIPPINGAMT',
    ];
    function __construct(
        \Tempocommerce\Paypal\Helper\Data $helper
    )
    {
        $this->helper = $helper;
    }

    public function beforeCall(\Magento\Paypal\Model\Api\Nvp $subject, $methodName, array $request)
    {
        $request['CURRENCYCODE'] = 'USD';
        if(isset($request['AMT'])){
            $request['AMT'] = (string)$this->helper->convert($request['AMT']);
        }
        foreach ($request as $key => $itemAmt){
            if(preg_match('/L_SHIPPINGOPTIONAMOUNT\d/',$key)){
                $request[$key] = (string)$this->helper->convert($itemAmt);
            }
        }
        foreach ($request as $key => $itemAmt){
            if(preg_match('/L_AMT\d/',$key)){
                $request[$key] = (string)$this->helper->convert($itemAmt);
            }
        }
        foreach ($this->_lineItemTotalExportMap as $key => $mapValue){
            if(isset($request[$mapValue])){
                $request[$mapValue] = (string)$this->helper->convert($request[$mapValue]);
            }
        }
        return [$methodName, $request];
    }
}