<?php
namespace Tempocommerce\Paypal\Plugin\Model\Cart;

class Totals
{

    protected $helper;

    function __construct(
        \Tempocommerce\Paypal\Helper\Data $helper
    )
    {
        $this->helper = $helper;
    }

    public function afterGetGrandTotal(\Magento\Quote\Model\Cart\Totals $subject, $result)
    {
        $subject->setData('grand_total_usd', $this->helper->convert($result));
        return $result;
    }
}