#!/bin/bash
## monitor media folder and optimize new images
/usr/bin/inotifywait -e create \
    -mrq --timefmt %a-%b-%d-%T --format '%w%f %T' \
    --excludei '\.(xml|php|phtml|html?|css|js|ico|te?mp|txt|csv|swp|sql|t?gz|zip|svn?g|git|log|ini|opt|prog|crush)~?' \
    /home/petitgourmetuae/public_html/pub/media | while read line; do
    echo "${line} " >> /home/petitgourmetuae/public_html/var/log/images_optimization.log
    FILE=$(echo ${line} | cut -d' ' -f1)
    TARGETEXT="(jpg|jpeg|png|gif)"
    EXTENSION="${FILE##*.}"
  if [[ "${EXTENSION}" =~ ${TARGETEXT} ]];
    then
   su petitgourmetuae -s /bin/bash -c "/home/petitgourmetuae/public_html/wesley.pl ${FILE} >/dev/null 2>&1"
  fi
done
