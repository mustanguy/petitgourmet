#!/bin/bash
git pull
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento cache:flush
chown -R petitgourmetuae:petitgourmetuae /home/petitgourmetuae/public_html/
service nginx restart && service php-fpm restart
chown -R petitgourmetuae:petitgourmetuae /home/petitgourmetuae/
